#include <stdio.h>
#include <math.h>

// function to wrap to 2pi
double wrap2pi(double x) {
    while (x < 0) {
        x = x + 2 * M_PI;
    }
    return fmod(x, 2*M_PI);
}

// function to wrap to 350
double wrap360(double x) {
    while (x < 0) {
        x = x + 360.0;
    }
    return fmod(x, 360.0);
}

// function to wrap to pi
double wrappi(double x) {
    while (x < 0) {
        x = x + 2 * M_PI;
    }
    return fmod(x, M_PI);
}

// Kepler's equation based on AJR matlab version
double KepEqn(double M, double e, const char key) {

    // solution tolerance
    double tol = 1e-13;

    // true anomaly
    double f_E;
    double f;

    // Eccentric Anomaly
    double E;
    double Enew;
    double Eold;

    // convert to radians if needed
    if (key == 'd') {
        M = M*d2r;
    }

    // a priori estimate for eccecntric anomaly
    if (-M_PI < M || M < 0 || M > M_PI) {
        Eold = M - e;        
    }
    else {
        Eold = M + e;
    }
    printf("Eold: %f\n",Eold);

    // Iterate with Newton Raphson
    f_E = Eold - e*sin(Eold) - M;
    printf("f_E: %f\n",f_E);

    while (fabs(f_E) > tol) {
        Enew = Eold - f_E/(1 - e*cos(Eold));
        f_E = Enew - e*sin(Enew) - M;
        Eold = Enew;
    }

    // converged eccentric anomaly in radians
    E = Enew;
    printf("E: %f\n",E);

    // True anomaly in radians
    // f = fmod(2*atan(sqrt((1 + e)/(1 - e))*tan(E/2.0)), 2.0*M_PI);
    f = wrap2pi(2*atan(sqrt((1 + e)/(1 - e))*tan(E/2.0)));
    printf("f: %f <- %f\n",f,2*atan(sqrt((1 + e)/(1 - e))*tan(E/2.0)));

    // convert to degrees if needed
    if (key == 'd') {
        f = f*r2d;
    }

    // return converged solution
    return f;
    
}

// function to convert Keplerian elements to Rosengren-Scheeres-Milankovitch type
void oe2eHlam(double oe[6], double mu, const char key, double e[3], double H[3], double *lam ) {

    // Classical orbit elements
    double a  = oe[0];
    double ec = oe[1];   
    double in = oe[2];   
    double Om = oe[3];   
    double w  = oe[4];    
    double M  = oe[5];
        
    if (key == 'd') {
        in = in*d2r;
        Om = Om*d2r;
        w  = w*d2r;
        M  = M*d2r;
    }  

    // mean orbital longitude
    *lam = wrap2pi(w + Om + M);

    if (key == 'd') {
        *lam = *lam*r2d;
    }

    // Eccentricity and angular momentum unit vectors
    double ehat[3];
    double hhat[3];

    ehat[0] = cos(w)*cos(Om) - cos(in)*sin(w)*sin(Om);
    ehat[1] = cos(w)*sin(Om) + cos(in)*sin(w)*cos(Om);
    ehat[2] = sin(in)*sin(w);

    hhat[0] = sin(in)*sin(Om);
    hhat[1] = -sin(in)*cos(Om);
    hhat[2] = cos(in);

    // Eccentricity and normalized angular momentum vectors
    e[0] = ec*ehat[0]; 
    e[1] = ec*ehat[1]; 
    e[2] = ec*ehat[2]; 

    H[0] = sqrt(mu*a*(1 - ec*ec))*hhat[0];
    H[1] = sqrt(mu*a*(1 - ec*ec))*hhat[1];
    H[2] = sqrt(mu*a*(1 - ec*ec))*hhat[2];

}

// function to convert the Milankovitch elements into the Keplerian set
void eHlam2coe(double e[3], double H[3], double *lam, const char key, double coe[6]) {

    // convert to radians
    if (key == 'd') {
        *lam = *lam*d2r;
    }

    // Eccentricity []
    double ec = normvec(e);

    // Semi-major axis [ km ]
    double a = normvec(H)*normvec(H)/MU/(1 - ec*ec);

    // Angular momentum unit vector
    double hhat[3];
    hhat[0] = H[0]/normvec(H);
    hhat[1] = H[1]/normvec(H);
    hhat[2] = H[2]/normvec(H);

    // Ascending node vector
    double nhat[3];
    double zhat[3];
    double temp[3];

    // z direction unit vector
    zhat[0] = 0;
    zhat[1] = 0;
    zhat[2] = 1;

    // compute temp vector
    iauPxp(zhat, H, temp);
    double tempnorm = normvec(temp);

    // compute n unit vector
    nhat[0] = temp[0]/tempnorm;
    nhat[1] = temp[1]/tempnorm;
    nhat[2] = temp[2]/tempnorm;

    printf("hhat:\n");
    printStackVec(hhat, 3);
    printf("zhat:\n");
    printStackVec(zhat, 3);
    printf("nhat:\n");
    printStackVec(nhat, 3);

    // Inclination [ rad ]
    double in = wrappi(acos(iauPdp(zhat, hhat)));
    printf("in: %.20f\n", in);

    // Longitude of the ascending node [ rad ]
    double Om = wrap2pi(atan2(nhat[1], nhat[0]));
    printf("Om: %.20f\n", Om);

    // Argument of periapsis [ rad ]
    iauPxp(nhat, e, temp);
    double w  = wrap2pi(atan2(iauPdp(hhat, temp), iauPdp(nhat, e)));
    printf("w: %.20f\n", w);

    // mean anomaly [ rad ] 
    double M = wrap2pi(*lam - w - Om);
    printf("M: %.20f <- %.20f\n", M, *lam - w - Om);

    // vector of orbital elements
    coe[0] = a;
    coe[1] = ec;
    coe[2] = in;
    coe[3] = Om;
    coe[4] = w;
    coe[5] = M;          

    // convert to degrees
    if (key == 'd') {
        coe[2]= coe[2]*r2d;
        coe[3]= coe[3]*r2d;
        coe[4]= coe[4]*r2d;
        coe[5]= coe[5]*r2d;
    }
}

// First order mapping between mean and osculating orbital elements
void mean2oscIRDG(double coe[6], const char key, double coep[6], const char input) {

    // local j2
    double j2;

    // if tranforming from osculating to mean
    if (input == 'o') {
        j2 = (0.001082626110749);
    }

    // convert to radians
    if (key == 'd') {
        coe[2]= coe[2]*d2r;
        coe[3]= coe[3]*d2r;
        coe[4]= coe[4]*d2r;
        coe[5]= coe[5]*d2r;
    }

    // compute true anomaly
    double ec = coe[1];
    double M  = coe[5];
    double f  = wrap2pi(KepEqn( M, ec, 'r'));
    printf("initial ec, M, f: %f, %f, %f, %f \n",ec, M, f, KepEqn( M, ec, 'r'));

    // Transform from classical to Milankovith elements
    double e[3];
    double H[3];
    double lam;

    printf("coe:\n");
    printStackVec(coe, 6);
    oe2eHlam(coe, MU, 'r', e, H, &lam);

    // Auxiliary quantities
    double hhat[3];
    double ehat[3]; 
    double Hmag = normvec(H);
    double p    = Hmag*Hmag/MU;
    double nu   = sqrt(1 - ec*ec);       
    hhat[0]     = H[0]/normvec(H);
    hhat[1]     = H[1]/normvec(H);
    hhat[2]     = H[2]/normvec(H);
    ehat[0]     = e[0]/normvec(e); 
    ehat[1]     = e[1]/normvec(e); 
    ehat[2]     = e[2]/normvec(e);

    // Auxiliary vector operations
    double eperp[3];
    double ehatdyad[3][3];
    double hhatdyad[3][3];
    double ehateperp[3][3];
    double eperpdyad[3][3];
    double eperpehat[3][3];
    iauPxp(hhat, ehat, eperp);
    outerProduct(hhat, hhat, hhatdyad);
    outerProduct(ehat, ehat, ehatdyad);
    outerProduct(ehat, eperp, ehateperp);
    outerProduct(eperp, eperp, eperpdyad);
    outerProduct(eperp, ehat, eperpehat);

    printf("H:\n");
    printStackVec(H, 3);
    printf("e:\n");
    printStackVec(e, 3);
    printf("lam: %f\n", lam);
    printf("hhat:\n");
    printStackVec(hhat, 3);
    printf("ehat:\n");
    printStackVec(ehat, 3);
    printf("eperp:\n");
    printStackVec(eperp, 3);
    printf("hhatdyad:\n");
    printStackArr3by3(hhatdyad);
    printf("ehatdyad:\n");
    printStackArr3by3(ehatdyad);
    printf("ehateperp:\n");
    printStackArr3by3(ehateperp);
    printf("eperpdyad\n");
    printStackArr3by3(eperpdyad);
    printf("eperpehat\n");
    printStackArr3by3(eperpehat);
    printf("\n");
    
    double phat[3];
    phat[0] = 0.0;
    phat[1] = 0.0;
    phat[2] = 1.0;
    printStackVec(phat, 3);

    double ptilde[3][3];
    ptilde[0][0] = 0.0;
    ptilde[0][1] = -1.0;
    ptilde[0][2] = 0.0;
    ptilde[1][0] = 1.0;
    ptilde[1][1] = 0.0;
    ptilde[1][2] = 0.0;
    ptilde[2][0] = 0.0;
    ptilde[2][1] = 0.0;
    ptilde[2][2] = 0.0;

    double sf  = sin(f);
    double s2f = sin(2.0*f);
    double s3f = sin(3.0*f);
    double s4f = sin(4.0*f);
    double s5f = sin(5.0*f);
    double cf  = cos(f);
    double c2f = cos(2.0*f);
    double c3f = cos(3.0*f);
    double c4f = cos(4.0*f);
    double c5f = cos(5.0*f);
    printf("\n sf, s2f, s3f, s4f, s5f:, %.20f, %.20f, %.20f, %.20f, %.20f\n",sf, s2f, s3f, s4f, s5f);
    printf("\n cf, c2f, c3f, c4f, c5f:, %.20f, %.20f, %.20f, %.20f, %.20f\n",cf, c2f, c3f, c4f, c5f);


    double X_001 = -ec;
    double X_002 = (ec*ec)*(1.0 + 2.0*nu)/((1.0 + nu)*(1.0+ nu));
    double X_003 = -(ec*ec*ec)*(1.0 + 3.0*nu)/((1.0 + nu)*(1.0+ nu)*(1.0+ nu));
    double X_004 = (ec*ec*ec*ec)*(1.0 + 4.0*nu)/((1.0 + nu)*(1.0+ nu)*(1.0+ nu)*(1.0+ nu));
    double X_005 = -(ec*ec*ec*ec*ec)*(1.0 + 5.0*nu)/((1.0 + nu)*(1.0+ nu)*(1.0+ nu)*(1.0+ nu)*(1.0+ nu));
    printf("\n X_001, X_002, X_003, X_004, X_005:, %.20f, %.20f, %.20f, %.20f, %.20f\n",X_001, X_002, X_003, X_004, X_005);

    double I_0 = -f - ec*sf;
    printf("\n I_0: %.20f\n",I_0);

    double I_1     = -1.0/12.0*(12.0*sf + 6.0*ec*(2.0*f + s2f) + (ec*ec)*(9.0*sf + s3f));
    double I_2     = -1.0/12.0*(12.0*(X_001 - cf) + 6.0*ec*(X_002 - c2f) + \
                        (ec*ec)*(X_003 + 3.0*X_001 - c3f - 3.0*cf));
    printf("\n I_1, I_2: %.20f, %.20f\n",I_1, I_2);
    
    double II_11   = -1.0/12.0*(3.0*(2.0*f + s2f) + ec*(9.0*sf + s3f));
    double II_12   = -1.0/12.0*(3.0*(X_002 - c2f) + ec*(3.0*X_001 + X_003 - 3.0*cf - c3f));
    double II_22   = -1.0/12.0*(3.0*(2.0*f - s2f) + ec*(3.0*sf - s3f));
    printf("\n II_11, II_12, II_22: %.20f, %.20f, %.20f\n",II_11, II_12, II_22);

    double III_111 = -1.0/96.0*(8.0*(9.0*sf + s3f) + 3.0*ec*(12.0*f + 8.0*s2f + s4f));
    double III_112 = -1.0/96.0*(8.0*(3.0*X_001 + X_003 - 3.0*cf - c3f) + \
                        3.0*ec*(4.0*X_002 + X_004 - 4.0*c2f - c4f));
    double III_122 = -1.0/96.0*(8.0*(3.0*sf - s3f) + 3.0*ec*(4.0*f - s4f));
    double III_222 = -1.0/96.0*(8.0*(9.0*X_001 - X_003 - 9.0*cf + c3f) + \
                        3.0*ec*(4.0*X_002 - X_004 - 4.0*c2f + c4f));
    printf("\n III_111, III_112, III_122, III_222: %.20f, %.20f, %.20f, %.20f\n",III_111, III_112, III_122, III_222);

        
    double IV_111  = -1.0/240.0*(20.0*(9.0*sf + s3f) + 15.0*ec*(12.0*f + 8.0*s2f + s4f) + \
                        (ec*ec)*(150.0*sf + 25.0*s3f + 3.0*s5f));      
    double IV_112  = -1.0/240.0*(20.0*(3.0*X_001 + X_003 - 3.0*cf - c3f) + \
                        15.0*ec*(4.0*X_002 + X_004 - 4.0*c2f - c4f) + \
                        3.0*(ec*ec)*(10.0*X_001 + 5.0*X_003 + X_005 - 10.0*cf - 5.0*c3f - c5f));
    double IV_122  = -1.0/240.0*(20.0*(3.0*sf - s3f) + 15.0*ec*(4.0*f - s4f) + \
                        (ec*ec)*(30.0*sf - 5.0*s3f - 3.0*s5f));     
    double IV_222  = -1.0/240.0*(20.0*(9.0*X_001 - X_003 - 9.0*cf + c3f) + \
                        15.0*ec*(4.0*X_002 - X_004 - 4.0*c2f + c4f) + \
                        (ec*ec)*(30.0*X_001 + 5.0*X_003 - 3.0*X_005 - 30.0*cf - 5.0*c3f + 3.0*c5f));
    printf("\n IV_111, IV_112, IV_122, IV_222: %.20f, %.20f, %.20f, %.20f\n",IV_111, IV_112, IV_122, IV_222);
      

    // Averaged short-periodic corrections
    double Dele[3];
    double DelH[3];
    double Dellam;
    double temp1[3];
    double temp2[3];
    double tempmat1[3][3];
    double tempmat2[3][3];
    double tempmat3[3][3];

    // matrix/vector operations for Dele
    iauRxp(ptilde, ehat, temp1);
    printf("ptilde * ehat:\n");
    printStackVec(temp1, 3);
    revRxp(hhat, ptilde, temp2);
    printf("hhat'*ptilde:\n");
    printStackVec(temp2, 3);

    Dele[0] = 3.0 * j2 * (REarth*REarth) / 2.0 / (p*p) * \
                ( - M / 2.0 * ec * ( ( 1.0 - 5.0 * (hhat[2]*hhat[2]) ) * eperp[0] + \
                2.0 * hhat[2] * temp1[0] ) + \
                I_1 * ( eperp[0] + 2.0 * ehat[2] * temp2[0] ) - \
                I_2 * ( ehat[0] - 2.0 * eperp[2] * temp2[0] ) - \
                5.0 * IV_111 * (ehat[2]*ehat[2]) * eperp[0] + \
                5.0 * IV_112 * ( (ehat[2]*ehat[2]) * ehat[0] - 2.0 * ehat[2] * eperp[2] * eperp[0] ) + \
                5.0 * IV_122 * ( 2.0 * ehat[2] * eperp[2] * ehat[0] - (eperp[2]*eperp[2]) * eperp[0] ) + \
                5.0 * IV_222 * (eperp[2]*eperp[2]) * ehat[0] - \
                2.0 * ec * II_11 * ehat[2] * eperp[2] * ehat[0] + \
                2.0 * ec * II_12 * ( ehat[2] * phat[0] - ehat[2] * eperp[2] * eperp[0] - (eperp[2]*eperp[2]) * ehat[0] ) + \
                2.0 * ec * II_22 * ( eperp[2] * phat[0] - (eperp[2]*eperp[2]) * eperp[0] ) + \
                2.0 * ( (ehat[2]*ehat[2]) - (eperp[2]*eperp[2]) ) * ( III_112 * ehat[0] + III_122 * eperp[0] ) + \
                2.0 * ehat[2] * eperp[2] * ( ( III_122 - III_111 ) * ehat[0] + ( III_222 - III_112 ) * eperp[0] ) );

    Dele[1] = 3.0 * j2 * (REarth*REarth) / 2.0 / (p*p) * \
                ( - M / 2.0 * ec * ( ( 1.0 - 5.0 * (hhat[2]*hhat[2]) ) * eperp[1] + \
                2.0 * hhat[2] * temp1[1] ) + \
                I_1 * ( eperp[1] + 2.0 * ehat[2] * temp2[1] ) - \
                I_2 * ( ehat[1] - 2.0 * eperp[2] * temp2[1] ) - \
                5.0 * IV_111 * (ehat[2]*ehat[2]) * eperp[1] + \
                5.0 * IV_112 * ( (ehat[2]*ehat[2]) * ehat[1] - 2.0 * ehat[2] * eperp[2] * eperp[1] ) + \
                5.0 * IV_122 * ( 2.0 * ehat[2] * eperp[2] * ehat[1] - (eperp[2]*eperp[2]) * eperp[1] ) + \
                5.0 * IV_222 * (eperp[2]*eperp[2]) * ehat[1] - \
                2.0 * ec * II_11 * ehat[2] * eperp[2] * ehat[1] + \
                2.0 * ec * II_12 * ( ehat[2] * phat[1] - ehat[2] * eperp[2] * eperp[1] - (eperp[2]*eperp[2]) * ehat[1] ) + \
                2.0 * ec * II_22 * ( eperp[2] * phat[1] - (eperp[2]*eperp[2]) * eperp[1] ) + \
                2.0 * ( (ehat[2]*ehat[2]) - (eperp[2]*eperp[2]) ) * ( III_112 * ehat[1] + III_122 * eperp[1] ) + \
                2.0 * ehat[2] * eperp[2] * ( ( III_122 - III_111 ) * ehat[1] + ( III_222 - III_112 ) * eperp[1] ) );

    Dele[2] = 3.0 * j2 * (REarth*REarth) / 2.0 / (p*p) * \
                ( - M / 2.0 * ec * ( ( 1.0 - 5.0 * (hhat[2]*hhat[2]) ) * eperp[2] + \
                2.0 * hhat[2] * temp1[2] ) + \
                I_1 * ( eperp[2] + 2.0 * ehat[2] * temp2[2] ) - \
                I_2 * ( ehat[2] - 2.0 * eperp[2] * temp2[2] ) - \
                5.0 * IV_111 * (ehat[2]*ehat[2]) * eperp[2] + \
                5.0 * IV_112 * ( (ehat[2]*ehat[2]) * ehat[2] - 2.0 * ehat[2] * eperp[2] * eperp[2] ) + \
                5.0 * IV_122 * ( 2.0 * ehat[2] * eperp[2] * ehat[2] - (eperp[2]*eperp[2]) * eperp[2] ) + \
                5.0 * IV_222 * (eperp[2]*eperp[2]) * ehat[2] - \
                2.0 * ec * II_11 * ehat[2] * eperp[2] * ehat[2] + \
                2.0 * ec * II_12 * ( ehat[2] * phat[2] - ehat[2] * eperp[2] * eperp[2] - (eperp[2]*eperp[2]) * ehat[2] ) + \
                2.0 * ec * II_22 * ( eperp[2] * phat[2] - (eperp[2]*eperp[2]) * eperp[2] ) + \
                2.0 * ( (ehat[2]*ehat[2]) - (eperp[2]*eperp[2]) ) * ( III_112 * ehat[2] + III_122 * eperp[2] ) + \
                2.0 * ehat[2] * eperp[2] * ( ( III_122 - III_111 ) * ehat[2] + ( III_222 - III_112 ) * eperp[2] ) );  

    printf("Dele:\n");
    printStackVec(Dele, 3);


    // vector/matrix operation for DelH

    // - M / 2 * hhatdyad
    cxR((-M/2), hhatdyad, tempmat1);
    printf("\n- M / 2 * hhatdyad:\n");
    printStackArr3by3(tempmat1);

    // II_11 * ehatdyad
    cxR(II_11, ehatdyad, tempmat2);
    printf("\n II_11 * ehatdyad:\n");
    printStackArr3by3(tempmat2);

    // - M / 2 * hhatdyad  + II_11 * ehatdyad
    RpR(tempmat1, tempmat2, tempmat3);
    printf("\n - M / 2 * hhatdyad  + II_11 * ehatdyad:\n");
    printStackArr3by3(tempmat3);

    // ehateperp + eperpehat
    RpR(ehateperp, eperpehat, tempmat1);
    printf("\n ehateperp + eperpehat:\n");
    printStackArr3by3(tempmat1);

    // II_12 * ( ehateperp + eperpehat )
    cxR(II_12, tempmat1, tempmat2);
    printf("\n II_12 * ( ehateperp + eperpehat ):\n");
    printStackArr3by3(tempmat2);

    // - M / 2 * hhatdyad + II_11 * ehatdyad + II_12 * ( ehateperp + eperpehat) 
    RpR(tempmat3, tempmat2, tempmat1);
    printf("\n  M / 2 * hhatdyad + II_11 * ehatdyad + II_12 * ( ehateperp + eperpehat) :\n");
    printStackArr3by3(tempmat1);

    // II_22 * eperpdyad
    cxR(II_22, eperpdyad, tempmat2);
    printf("\n II_22 * eperpdyad:\n");
    printStackArr3by3(tempmat2);

    // ( - M / 2 * hhatdyad + II_11 * ehatdyad + II_12 * ( ehateperp + eperpehat ) + II_22 * eperpdyad )
    RpR(tempmat1, tempmat2, tempmat3);
    printf("\n ( - M / 2 * hhatdyad + II_11 * ehatdyad + II_12 * ( ehateperp + eperpehat ) + II_22 * eperpdyad ):\n");
    printStackArr3by3(tempmat3);

    // ( - M / 2 * hhatdyad + II_11 * ehatdyad + II_12 * ( ehateperp + eperpehat ) + II_22 * eperpdyad ) * ptilde
    iauRxr(tempmat3, ptilde, tempmat1);
    printf("\n ( - M / 2 * hhatdyad + II_11 * ehatdyad + II_12 * ( ehateperp + eperpehat ) + II_22 * eperpdyad ) * ptilde:\n");
    printStackArr3by3(tempmat1);


    revRxp(phat, tempmat1, temp1);
    printf("phat:\n");
    printStackVec(phat, 3);
    printf("\n phat * ( - M / 2 * hhatdyad + II_11 * ehatdyad + II_12 * ( ehateperp + eperpehat ) + II_22 * eperpdyad ) * ptilde:\n");
    printStackVec(temp1, 3);
    
    DelH[0] = -3.0 * Hmag * j2 * (REarth*REarth) / (p*p) * temp1[0];
    DelH[1] = -3.0 * Hmag * j2 * (REarth*REarth) / (p*p) * temp1[1];  
    DelH[2] = -3.0 * Hmag * j2 * (REarth*REarth) / (p*p) * temp1[2]; 

    printf("DelH:\n");
    printStackVec(DelH, 3);              

    Dellam = 3.0 * j2 * (REarth*REarth) / 2.0 / (p*p) * \
                ( M / 2.0 * ( nu * ( 3.0 * (hhat[2]*hhat[2]) - 1.0 ) + 5.0 * (hhat[2]*hhat[2]) - 2.0 * hhat[2] - 1.0 ) + \
                2.0 * nu * I_0 + \
                ec / ( 1.0 + nu ) * ( I_1 - 3.0 * ( IV_111 * (hhat[2]*hhat[2]) + \
                2.0 * IV_112 * ehat[2] * eperp[2] +  IV_122 * (eperp[2]*eperp[2]) ) + \
                2.0 * ( III_222 - III_112 + IV_222 - IV_112 ) * ehat[2] * eperp[2] + \
                2.0 * ( III_122 + IV_122 ) * ( (ehat[2]*ehat[2]) - (eperp[2]*eperp[2]) ) ) - \
                2.0 * ( 3.0 * nu + hhat[2] / ( 1.0 + hhat[2] ) ) * ( II_11 * (ehat[2]*ehat[2]) + \
                2.0 * II_12 * ehat[2] * eperp[2] + II_22 * (eperp[2]*eperp[2]) ) );

    printf("initial Hmag, p, nu: %f, %f, %f \n",Hmag, p, nu); 
    printf("Dellam: %.20f\n", Dellam);
            
    // Transformed orbit element vector    
    iauPmp(e, Dele, temp1);
    iauPmp(H, DelH, temp2);
    e[0] = temp1[0];
    e[1] = temp1[1];
    e[2] = temp1[2];
    H[0] = temp2[0];
    H[1] = temp2[1]; 
    H[2] = temp2[2];  
    lam = lam - Dellam;

    printf("e:\n");
    printStackVec(e, 3);

    printf("H:\n");
    printStackVec(H, 3);

    printf("lam: %.20f\n", lam);

    // tranform to degrees
    // if (key == 'd') {
    //     lam = lam*r2d;
    // }

    // Transform to classical orbital elements
    eHlam2coe(e, H, &lam, 'r', coe);

    // add first elements
    coep[0] = coe[0];
    coep[1] = coe[1];

    // tranform to degrees if necessary
    if (key == 'd') {
        coep[2]= coe[2]*r2d;
        coep[3]= coe[3]*r2d;
        coep[4]= coe[4]*r2d;
        coep[5]= coe[5]*r2d;
    }

}
