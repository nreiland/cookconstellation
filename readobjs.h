// helper function to parse lines from target/field data list
void parseline(char line[], OBJS *list, const char key, const char elType){

    // parsing parameters
    const char delim[2] = ",";
    char *eptr;
    char *token;
    int length = 100;

    // individual strings for parameters
    char mjd_str[100]; 
    char SMA_str[100];
    char ECC_str[100];
    char INC_str[100];
    char RAAN_str[100];
    char ARGP_str[100];
    char MA_str[100];
    char SCMass_str[100];
    char Adrag_str[100];
    char ASRP_str[100];
    char CD_str[100];
    char CR_str[100];

    // copy from text file        
    token = strtok(line, delim);
    strcpy(mjd_str, token);
    token = strtok(NULL, delim);
    strcpy(SMA_str, token);
    token = strtok(NULL, delim);
    strcpy(ECC_str, token);
    token = strtok(NULL, delim);
    strcpy(INC_str, token);
    token = strtok(NULL, delim);
    strcpy(RAAN_str, token);
    token = strtok(NULL, delim);
    strcpy(ARGP_str, token);
    token = strtok(NULL, delim);
    strcpy(MA_str, token);

    if (key == 'p') {
        token = strtok(NULL, delim);
        strcpy(SCMass_str, token);
        token = strtok(NULL, delim);
        strcpy(Adrag_str, token);
        token = strtok(NULL, delim);
        strcpy(ASRP_str, token);
        token = strtok(NULL, delim);
        strcpy(CD_str, token);
        token = strtok(NULL, delim);
        strcpy(CR_str, token);
    }

    if (elType == 'o') {

        // convert strings to doubles
        list->mjd0      = strtod(mjd_str, &eptr);
        list->COEosc[0] = strtod(SMA_str, &eptr);
        list->COEosc[1] = strtod(ECC_str, &eptr);
        list->COEosc[2] = strtod(INC_str, &eptr);
        list->COEosc[3] = strtod(RAAN_str, &eptr);
        list->COEosc[4] = strtod(ARGP_str, &eptr);
        list->COEosc[5] = strtod(MA_str, &eptr);

    }
    else if (elType == 'm') {

        // convert strings to doubles
        list->mjd0      = strtod(mjd_str, &eptr);
        list->COEmean[0] = strtod(SMA_str, &eptr);
        list->COEmean[1] = strtod(ECC_str, &eptr);
        list->COEmean[2] = strtod(INC_str, &eptr);
        list->COEmean[3] = strtod(RAAN_str, &eptr);
        list->COEmean[4] = strtod(ARGP_str, &eptr);
        list->COEmean[5] = strtod(MA_str, &eptr);

    }
    else {
        printf("PLEASE ENTER VALID ELEMENT TYPE ('o' or 'm')\n");
        exit(EXIT_FAILURE);
    }

    if (key == 'p') {
        list->SCMass    = strtod(SCMass_str, &eptr);
        list->Adrag     = strtod(Adrag_str, &eptr);
        list->ASRP      = strtod(ASRP_str, &eptr);
        list->CD        = strtod(CD_str, &eptr);
        list->CR        = strtod(CR_str, &eptr);
    }
}

// helper function to print object values
OBJS *printobjvals(OBJS *node, const char key, const char p) {

    if (p == 'p') {

        if (key == 'a') {

            printf("%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n",\
                node->mjd0, node->COEmean[0], node->COEmean[1], node->COEmean[2],\
                node->COEmean[3], node->COEmean[4], node->COEmean[5],\
                node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
        }
        else if (key == 'b') {

            printf("%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n",\
                node->mjd0, node->COEoscf[0], node->COEoscf[1], node->COEoscf[2],\
                node->COEoscf[3], node->COEoscf[4], node->COEoscf[5],\
                node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
        }
        else if (key == 'c') {

            printf("%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n",\
                node->mjd0, node->COEmeanf[0], node->COEmeanf[1], node->COEmeanf[2],\
                node->COEmeanf[3], node->COEmeanf[4], node->COEmeanf[5],\
                node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
        }
        else {

            printf("%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f\n",\
                node->mjd0, node->COEosc[0], node->COEosc[1], node->COEosc[2],\
                node->COEosc[3], node->COEosc[4], node->COEosc[5],\
                node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
        }

        return node->next;
    }
    else {

        if (key == 'a') {

            printf("%f, %f, %f, %f, %f, %f, %f\n",\
                node->mjd0, node->COEmean[0], node->COEmean[1], node->COEmean[2],\
                node->COEmean[3], node->COEmean[4], node->COEmean[5]);
        }
        else if (key == 'b') {

            printf("%f, %f, %f, %f, %f, %f, %f\n",\
                node->mjd0, node->COEoscf[0], node->COEoscf[1], node->COEoscf[2],\
                node->COEoscf[3], node->COEoscf[4], node->COEoscf[5]);
        }
        else if (key == 'c') {

            printf("%f, %f, %f, %f, %f, %f, %f\n",\
                node->mjd0, node->COEmeanf[0], node->COEmeanf[1], node->COEmeanf[2],\
                node->COEmeanf[3], node->COEmeanf[4], node->COEmeanf[5]);
        }
        else {

            printf("%f, %f, %f, %f, %f, %f, %f\n",\
                node->mjd0, node->COEosc[0], node->COEosc[1], node->COEosc[2],\
                node->COEosc[3], node->COEosc[4], node->COEosc[5]);
        }

        return node->next;
    }

}

// helper functino to print entire list
void printallObjvals(OBJS *node, const char key, const char p) {
    
    if (node == NULL) {
        printf("END OF LIST\n");
    }
    else {
        printallObjvals(printobjvals(node, key, p), key, p);
    }
    
}

// helper function to read text file lines into linked list
OBJS *readobjLines(FILE *fp, OBJS *current, const char key, const char elType) {

    // file line
    char line[1000];

    if (current == NULL){

        if (fgets(line, sizeof(line), fp)) {

            // allocate new node
            current = malloc(sizeof(OBJS));
            current->idx = 0;
            current->head = current;
            parseline(line, current, key, elType);
            current->next = readobjLines(fp, current, key, elType);
            current->tail = current->next->tail;
            return current;

        }
        else {

            printf("end of file, no lines\n");
            current->next = NULL;
            current->tail = current;
            return current;

        }
    }
    else {

        if (fgets(line, sizeof(line), fp)) {

            // allocate new node
            OBJS *node = malloc(sizeof(OBJS));
            node->idx = current->idx + 1;
            node->head = current->head;
            parseline(line, node, key, elType);
            node->next = readobjLines(fp, node, key, elType);

            if (node->next == NULL) {

                node->tail = node;

            }
            else {

                node->tail = node->next->tail;

            }
            return node;
        }
        else {

            return NULL;

        }

    } 

}

// function to read in target/field data
OBJS *readobjs(char inputFile[], const char key, const char elType) {
    FILE *fp = fopen(inputFile, "r");
    OBJS *objs;
    objs = NULL;
    objs = readobjLines(fp, objs, key, elType);
    fclose(fp);
    return objs->head;
}

