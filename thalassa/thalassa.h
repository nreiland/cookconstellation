// thalassa input type
typedef struct {
   double MJD0;
   double COE0[6]; 
   double tspan; 
   double tstep; 
   int insgrav; 
   int isun; 
   int imoon; 
    int idrag; 
    int iF107; 
    int iSRP; 
    int iephem; 
    int gdeg; 
    int gord; 
    double tol; 
    int imcoll;
    int eqs; 
    double SCMass; 
    double ADrag; 
    double ASRP; 
    double CD; 
    double CR; 
    int npts; 
    double *cart;
    double *orbs; 
    int tag;
    int exitcode;  
} thalassaInput;

// function declarations
extern void thalassasub_(double *MJD0, double *COE0, double *tspan, double *tstep,
                            int *insgrav, int *isun, int *imoon, int *idrag, int *iF107, 
                            int *iSRP, int *iephem, int *gdeg, int *gord, int *rmxstep, 
                            double *tol, int *imcoll,int *eqs, double *SCMass, double *ADrag, 
                            double *ASRP, double *CD, double *CR, int *mxpts, int *npts, 
                            double cart[], double orbs[], int *tag, int *exitcode);

// function to call thalassa
void thalassa(thalassaInput *in);
