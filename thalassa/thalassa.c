#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "thalassa.h"

/*main function*/
void thalassa(thalassaInput *in) {

    // tag
    int tag = 1;

    // calculate mxpts
    int tratio = in->tspan / in->tstep;
    int mxpts = tratio + 5000;

    // allocate memory for trajectory
    in->cart = (double *)malloc(mxpts*7*sizeof(double));
    in->orbs = (double *)malloc(mxpts*7*sizeof(double));

    // propagate
    // printf("mxpts = %d, tspan = %f, tstep = %f, tratio = %d\n",mxpts, in->tspan, in->tstep, tratio);
    thalassasub_(&in->MJD0, in->COE0, &in->tspan, &in->tstep, &in->insgrav, &in->isun, 
                    &in->imoon, &in->idrag, &in->iF107, &in->iSRP, &in->iephem, &in->gdeg, 
                    &in->gord, &mxpts, &in->tol, &in->imcoll, &in->eqs, &in->SCMass, &in->ADrag, 
                    &in->ASRP, &in->CD, &in->CR, &mxpts, &in->npts, in->cart, in->orbs, &tag, 
                    &in->exitcode);

    // check if mxpts was reached
    while (in->exitcode == -3)
    {
        // update max number of points
        mxpts += 50000;

        // repropagate
        thalassasub_(&in->MJD0, in->COE0, &in->tspan, &in->tstep, &in->insgrav, &in->isun, 
                    &in->imoon, &in->idrag, &in->iF107, &in->iSRP, &in->iephem, &in->gdeg, 
                    &in->gord, &mxpts, &in->tol, &in->imcoll, &in->eqs, &in->SCMass, &in->ADrag, 
                    &in->ASRP, &in->CD, &in->CR, &mxpts, &in->npts, in->cart, in->orbs, &tag, 
                    &in->exitcode);
    }

}