spice=~/Documents/codes/fortran/SPICE/toolkit/lib/spicelib.a
sofa=~/Documents/codes/fortran/SOFA/src/libsofa.a
gccl=/usr/local/gfortran/lib/gcc/x86_64-apple-darwin16/6.3.0/libgcc.a
gfl=/usr/local/gfortran/lib/libgfortran.a
qml=/usr/local/gfortran/lib/libquadmath.a

gcc -O -o genCook.x genCook.c SOFA/libsofa_c.a kissfft/libkissfft.a thalassa/libthalassa.a $spice $sofa $gccl $gfl $qml
