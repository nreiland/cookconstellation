#include <stdio.h>
#include <stdlib.h>

// recursive helper function to write element list
void elementListHelper(OBJS *node, FILE *fobj, const char elType, const char key) {


    // not end of list
    if (node != NULL) {

        // nominal osculating elements
        if (elType == 'o') {

            if (key == 'p') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f,"
                        "%.15f, %.15f, %.15f\n", node->mjd0, node->COEosc[0], node->COEosc[1], 
                        node->COEosc[2], node->COEosc[3], node->COEosc[4], node->COEosc[5], 
                        node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
            }
            else if (key == 'n') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n", node->mjd0, 
                        node->COEosc[0], node->COEosc[1], node->COEosc[2], node->COEosc[3],
                        node->COEosc[4], node->COEosc[5]);
            }
            else {
                printf("PLEASE ENTER VALID KEY (either `p` to print physical parameters \
                        or 'n' to not print physical parameters)\n");
                exit(EXIT_FAILURE);
            }
        }

        // nominal mean elements
        else if (elType == 'm') {

            if (key == 'p') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f,"
                        "%.15f, %.15f, %.15f\n", node->mjd0, node->COEmean[0], node->COEmean[1], 
                        node->COEmean[2], node->COEmean[3], node->COEmean[4], node->COEmean[5], 
                        node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
            }
            else if (key == 'n') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n", node->mjd0, 
                        node->COEmean[0], node->COEmean[1], node->COEmean[2], node->COEmean[3],
                        node->COEmean[4], node->COEmean[5]);
            }
            else {
                printf("PLEASE ENTER VALID KEY (either `p` to print physical parameters \
                        or 'n' to not print physical parameters)\n");
                exit(EXIT_FAILURE);
            }
        }

        // frozen osculating elments
        else if (elType == 'f') {

            if (key == 'p') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f,"
                        "%.15f, %.15f, %.15f\n", node->mjd0, node->COEoscf[0], node->COEoscf[1], 
                        node->COEoscf[2], node->COEoscf[3], node->COEoscf[4], node->COEoscf[5], 
                        node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
            }
            else if (key == 'n') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n", node->mjd0, 
                        node->COEoscf[0], node->COEoscf[1], node->COEoscf[2], node->COEoscf[3],
                        node->COEoscf[4], node->COEoscf[5]);
            }
            else {
                printf("PLEASE ENTER VALID KEY (either `p` to print physical parameters \
                        or 'n' to not print physical parameters)\n");
                exit(EXIT_FAILURE);
            }
        }

        // frozen mean elments
        else if (elType == 'z') {

            if (key == 'p') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f,"
                        "%.15f, %.15f, %.15f\n", node->mjd0, node->COEmeanf[0], node->COEmeanf[1], 
                        node->COEmeanf[2], node->COEmeanf[3], node->COEmeanf[4], node->COEmeanf[5], 
                        node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
            }
            else if (key == 'n') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n", node->mjd0, 
                        node->COEmeanf[0], node->COEmeanf[1], node->COEmeanf[2], node->COEmeanf[3],
                        node->COEmeanf[4], node->COEmeanf[5]);
            }
            else {
                printf("PLEASE ENTER VALID KEY (either `p` to print physical parameters \
                        or 'n' to not print physical parameters)\n");
                exit(EXIT_FAILURE);
            }
        }

        // optimal frozen
        else if (elType == 'l') {

            if (key == 'p') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f,"
                        "%.15f, %.15f, %.15f\n", node->mjd0, node->COEopti[0], node->COEopti[1], 
                        node->COEopti[2], node->COEopti[3], node->COEopti[4], node->COEopti[5], 
                        node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
            }
            else if (key == 'n') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n", node->mjd0, 
                        node->COEopti[0], node->COEopti[1], node->COEopti[2], node->COEopti[3],
                        node->COEopti[4], node->COEopti[5]);
            }
            else {
                printf("PLEASE ENTER VALID KEY (either `p` to print physical parameters \
                        or 'n' to not print physical parameters)\n");
                exit(EXIT_FAILURE);
            }
        }

        // optimal frozen mean
        else if (elType == 'q') {

            if (key == 'p') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f,"
                        "%.15f, %.15f, %.15f\n", node->mjd0, node->COEopAvg[0], node->COEopAvg[1], 
                        node->COEopAvg[2], node->COEopAvg[3], node->COEopAvg[4], node->COEopAvg[5], 
                        node->SCMass, node->Adrag, node->ASRP, node->CD, node->CR);
            }
            else if (key == 'n') {
                fprintf(fobj,"%.15f, %.15f, %.15f, %.15f, %.15f, %.15f, %.15f\n", node->mjd0, 
                        node->COEopAvg[0], node->COEopAvg[1], node->COEopAvg[2], node->COEopAvg[3],
                        node->COEopAvg[4], node->COEopAvg[5]);
            }
            else {
                printf("PLEASE ENTER VALID KEY (either `p` to print physical parameters \
                        or 'n' to not print physical parameters)\n");
                exit(EXIT_FAILURE);
            }
        }


        // failure
        else {
            printf("PLEASE ENTER VALID ELEMENT TYPE (options are 'o', 'm', 'f', 'l'\
                    corresponding to nominal osculating, nominal mean, frozen \
                    osculating, and frozen mean, optimal frozen)\n");
            exit(EXIT_FAILURE);
        }


        // recurse
        elementListHelper(node->next, fobj, elType, key);

    }

}

// Function to write a type of elements from a linked list of objects to an output 
// file. Options for element type are osculating = 'o', mean = 'm', 
// frozen-osculating = 'f', frozen-mean = 'z', optimal-frozen = 'l', 
// and optimal-frozen-mean = 'q'. Options for key are
// 'p' = print physical parameters, 'n' = don't print physical parameters.
void writeElementList(OBJS *node, char fout[], const char elType, const char key) {

    //  open file
    FILE *fobj = fopen(fout, "w");

    // call recursive function
    elementListHelper(node->head, fobj, elType, key);

}