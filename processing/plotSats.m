function f = plotSats(plotSets, colorIn)

f = figure;
d2r = pi/180;
load 'Earth.mat';
PlotPlanet([0;0;0], planet.radius, planet);
set(gca,'color','w')
set(gcf,'color','w')
hold on
axis equal

nSets = length(plotSets);

for j = 1:nSets
    
    inputfile = plotSets{j};
    readOrbels = dlmread(inputfile,',');

    % convert to rads and grab only what we need
    sats = size(readOrbels,1);
    for idx = 1:sats
        a = readOrbels(idx,2);
        e = readOrbels(idx,3);
        i = readOrbels(idx,4);
        W = readOrbels(idx,5);
        w = readOrbels(idx,6);
        M = readOrbels(idx,7);
        orbels_rad(idx,1) = a;
        orbels_rad(idx,2) = i*d2r;
        orbels_rad(idx,3) = W*d2r;
        orbels_rad(idx,4) = w*d2r;
        orbels_rad(idx,5) = e;
        orbels_rad(idx,6) = M*d2r;
    end


    % plot orbits and satellites
    for i = 1:sats
        r = RVFromKepler(orbels_rad(i,:));
        plot3( r(1,1), r(2,1),r(3,1),'.','MarkerSize', 12,'color', colorIn{j}); hold on
        plot3( r(1,:), r(2,:),r(3,:),'color', [0 0 0], 'LineWidth', 0.001); hold on
    end
    
end