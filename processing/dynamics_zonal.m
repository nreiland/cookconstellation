function coe = dynamics_zonal(t, coe0, const, inputtype, angletype, odeopts)

%{
FILENAME:  mean2osc_IRDG

DESCRIPTION:  First-order mapping (based on the theory developed by Kozai,
but using the Milankovitch variables) between mean (orbit averaged, with
short- and long-period motion removed) orbit elements and osculating
(instantaneous) orbit elements. 

INPUTS:
    t           = n-dimensional vector of time instants for the outputs [s]
    coe0        = Initial orbital elements, [ a0; e0; i0; Om0; w0; M0 ]
    const       = structure containing primary body constants 
                  const.mu [ km^3/s^2 ]
                  const.R  [ km ]
                  const.J  [ ]
                  const.Nquad [ ]
    inputtype   = string indicating if input is mean or osculating orbit
                  elements 
                  'mean' = mean, 'osc' = osculating
    angletype   = string indicating desired angletype for inputs/outputs
                  'deg' = degrees, 'rad' = radians
    odeopts     = options of the integrator (generated with odeset)

OUTPUTS:
    coe         = 6 x n matrix containing classical orbit elements at t

MODIFICATIONS:
    Apr-2020    |    Lamberto - Original
%}



%  Conversions
d2r             = pi / 180;

if(strcmp(angletype, 'deg'))
    coe0(3 : 6) = coe0(3 : 6) * d2r;
end

a0              = coe0(1);        % semi-major axis [ km ]
e0              = coe0(2);        % eccentricity [ ]
i0              = coe0(3);        % inclination [ rad ]
Om0             = coe0(4);        % right ascension of the ascending node [ rad ]
w0              = coe0(5);        % argument of periapsis [ rad ]
M0              = coe0(6);        % mean anomaly at epoch time t0 [ rad ]

% Initial equinoctial elements
ti0              = tan(i0 / 2);

P10             = e0 .* sin(w0 + Om0);
P20             = e0 .* cos(w0 + Om0);
Q10             = ti0 .* sin(Om0);
Q20             = ti0 .* cos(Om0);
l0              = M0 + w0 + Om0;

coeeq0          = [a0; P10; P20; Q10; Q20; l0];


if(strcmp(inputtype, 'mean'))
    if isfield(const, 'Nquad'),
        Nquad   = const.Nquad;
    else
        Nquad   = 361;
    end
    [kn, we]    = lgwt(Nquad, - pi, pi);
    we          = we / sum(we);
    
    fdyn        = @(t, x) favg(x, const, kn, we);
else
    fdyn        = @(t, x) fosc(x, const);
end

[~, coeeq]      = ode113(fdyn, t, coeeq0, odeopts);
coeeq           = coeeq';

% Converting to classical elements
a               = coeeq(1, :);
e               = sqrt(coeeq(2, :).^2 + coeeq(3, :).^2);
i               = 2 * atan(sqrt(coeeq(4, :).^2 + coeeq(5, :).^2));
Om              = atan2(coeeq(4, :), coeeq(5, :));
Om(Om < 0)      = Om(Om < 0) + 2 * pi;
w               = mod(atan2(coeeq(2, :), coeeq(3, :)) - Om, 2 * pi);
w(w < 0)        = w(w < 0) + 2 * pi;
M               = mod(coeeq(6, :) - w - Om, 2 * pi);
M(M < 0)        = M(M < 0) + 2 * pi;

coe            = [a; e; i; Om; w; M];

if(strcmp(angletype, 'deg'))
    coe(3 : 6, :) = coe(3 : 6, :) / d2r;
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% AVERAGING %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dx    = favg(x, const, knots, weights) 

aux1           = ones(1, length(weights));
xKn            = x * aux1;
xKn(end, :)    = x(end) + knots;

dxKn           = fosc(xKn, const);

dx             = dxKn * weights;

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GVE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dcoe  = fosc(coe, const)

a              = coe(1, :);
P1             = coe(2, :);
P2             = coe(3, :);
Q1             = coe(4, :);
Q2             = coe(5, :);
l              = coe(6, :);

% Constants
mu             = const.mu;

% Useful variables
L              = keplerequin(l, P2, P1);     % true longitude [ rad ]
sL             = sin(L);
cL             = cos(L);
n              = sqrt(mu ./ a.^3);
e2             = P1.^2 + P2.^2;
p              = a .* (1 - e2);
b              = a .* sqrt(1 - e2);
h              = sqrt(mu * p);
por            = 1 + P1 .* sL + P2 .* cL;
r              = p ./ (1 + P1 .* sL + P2 .* cL);
roh            = r ./ h;

% LVLH frame
Q22            = Q2.^2;
Q11            = Q1.^2;
Q12            = 2 * Q1 .* Q2;
eta            = 1 + Q22 + Q11;

rDir           = zeros(3, length(r));                % Radial direction
rDir(1, :)     = ((1 - Q11 + Q22) .* cL + Q12 .* sL) ./ eta;
rDir(2, :)     = ((1 + Q11 - Q22) .* sL + Q12 .* cL) ./ eta;
rDir(3, :)     = 2 * (- Q1 .* cL + Q2 .* sL) ./ eta;
R              = rDir .* ([1; 1; 1] * r);

VxPFdir      = - (P1 + sL);
VyPFdir      = (P2 + cL);
vDir         = zeros(3, length(r));
vDir(1, :)   = ((1 - Q11 + Q22) .* VxPFdir + Q12 .* VyPFdir) ./ eta;
vDir(2, :)   = ((1 + Q11 - Q22) .* VyPFdir + Q12 .* VxPFdir) ./ eta;
vDir(3, :)   = 2 * (- Q1 .* VxPFdir + Q2 .* VyPFdir) ./ eta;

hDir         = normc(cross(rDir, vDir));             % Normal direction

tDir         = cross(hDir, rDir);                    % Tangential direction

% Force components in the tangential-normal frame
f            = grav(R, const);
Fr           = sum(rDir .* f, 1);
Ft           = sum(tDir .* f, 1);
Fh           = sum(hDir .* f, 1);

% Gauss Variational Equations
dadt        = 2 .* a.^2 .* ((P2 .* sL - P1 .* cL) .* Fr + por .* Ft) ./ h;
dP1dt       = roh .* (- por .* cL .* Fr + (P1 + (1 + por) .* sL) .* Ft - P2 .* (Q1 .* cL - Q2 .* sL) .* Fh);
dP2dt       = roh .* (por .* sL .* Fr + (P2 + (1 + por) .* cL) .* Ft + P1 .* (Q1 .* cL - Q2 .* sL) .* Fh);
dQ1dt       = 0.5 * roh .* (1 + Q1.^2 + Q2.^2) .* sL .* Fh;
dQ2dt       = 0.5 * roh .* (1 + Q1.^2 + Q2.^2) .* cL .* Fh;
dldt        = n - roh .* ((a .* por .* (P1 .* sL + P2 .* cL) ./ (a + b) + 2 * b ./ a) .* Fr ...
              + a .* (1 + por) .* (P1 .* cL - P2 .* sL) .* Ft ./ (a + b) + (Q1 .* cL - Q2 .* sL) .* Fh);

dcoe        = [dadt; dP1dt; dP2dt; dQ1dt; dQ2dt; dldt];

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%% KEPLER'S EQUATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%
function L = keplerequin(l, ex, ey) 

%      
%     Mean to true longitude computation
%     Newton method
%

eta       = sqrt(1 - ex.^2 - ey.^2);
AA        = 1 - ex.^2 ./ (1 + eta);
BB        = ex .* ey ./ (1 + eta);
CC        = 1 - ey.^2 ./ (1 + eta);

K         = l;                           % Initialization
cK        = cos(K);
sK        = sin(K);

f         = 1;
while max(abs(f)) > 1e-13,
  f       = K + ey .* cK - ex .* sK - l;
  df      = 1 - ey .* sK - ex .* cK;

  K       = K - f ./ df;
  cK      = cos(K);
  sK      = sin(K);
end


sL        = (AA .* sK + BB .* cK - ey) ./ (1 - ey .* sK - ex .* cK);
cL        = (BB .* sK + CC .* cK - ex) ./ (1 - ey .* sK - ex .* cK);

L         = atan2(sL, cL);               % True longitude
end


%%%%%%%%%%%%%%%%%%%%%%%%%%% GRAVITATIONAL FORCE %%%%%%%%%%%%%%%%%%%%%%%%%%%
function Fg = grav(rECEF, const)

% 
% Fg = gravity(rECEF, parameters)
% 
% Compute the perturbation force (per unit of mass) due to the zonal 
% gravity field. 
% 
% Inputs:
%   - rECEF      = 3xNt matrix defining the position vector in the ECEF
%                  frame at Nt times [m]
%   - const      = structured variable.
%
% Outputs:
%   - Fg         = 3xNt matrix defining the gravity force per unit of
%                  mass in the ECEF frame at Nt times [N / kg]
% 
% Ref: Montenbruck (WGS84 EGM96)
% 
% Lamberto Dell'Elce
% 

mu                  = const.mu;
Re                  = const.R;
J                   = [0; const.J(:)]';
N                   = length(J);

Nr                  = size(rECEF, 2);



% Useful variables
x                   = rECEF(1, :);
y                   = rECEF(2, :);
z                   = rECEF(3, :);

r                   = sqrt(x.^2 + y.^2 + z.^2);
Re2                 = Re^2;
r2                  = r.^2;
mu_Re2              = mu / Re2;
Re_r2               = Re ./ r2;
x_Re_r2             = x .* Re_r2;
y_Re_r2             = y .* Re_r2;
z_Re_r2             = z .* Re_r2;
Re2_r2              = Re2 ./ r2;

% Fg computation
Vz                  = zeros(N + 2, Nr);
Vz(1, :)            = Re ./ r;
Vz(2, :)            = z_Re_r2 .* Vz(1, :);

for n               = 2 : (N + 1),
    Vz(n + 1, :)    = ((2 * n - 1) * z_Re_r2 .* Vz(n, :) - (n - 1) * Re2_r2 .* Vz(n - 1, :)) / n;
end

Vx                  = zeros(N + 2, Nr);
Vy                  = zeros(N + 2, Nr);

    
Vx(2, :)            = x_Re_r2 .* Vz(1, :);
Vy(2, :)            = y_Re_r2 .* Vz(1, :);
    
Vx(3, :)            = 3 * z_Re_r2 .* Vx(2, :);
Vy(3, :)            = 3 * z_Re_r2 .* Vy(2, :);         

for n               = 3 : (N + 1),
    Vx(n + 1, :)    = ((2 * n - 1) * z_Re_r2 .* Vx(n, :) - n * Re2_r2 .* Vx(n - 1, :)) / (n - 1);
    Vy(n + 1, :)    = ((2 * n - 1) * z_Re_r2 .* Vy(n, :) - n * Re2_r2 .* Vy(n - 1, :)) / (n - 1);
end



% Evaluating the force
Fg                  = mu_Re2 * [J * Vx(3 : end, :); ...
                                J * Vy(3 : end, :); ...
                                ((2 : N + 1) .* J) * Vz(3 : end, :)];

end


%%%%%%%%%%%%%%%%%%%%%%%% LEGENDRE-GAUSS QUADRATURE %%%%%%%%%%%%%%%%%%%%%%%%

% normc

function out = normc(in)
    cols = size(in,2);
    rows = size(in,1);
    out = zeros(rows,cols);
    for j = 1:cols
        normval = norm(in(:,j));
        for i = 1:rows
            out(i,j) = in(i,j)/normval;
        end
    end
end

function [x,w]=lgwt(N,a,b)

% lgwt.m
%
% This script is for computing definite integrals using Legendre-Gauss 
% Quadrature. Computes the Legendre-Gauss nodes and weights  on an interval
% [a,b] with truncation order N
%
% Suppose you have a continuous function f(x) which is defined on [a,b]
% which you can evaluate at any x in [a,b]. Simply evaluate it at all of
% the values contained in the x vector to obtain a vector f. Then compute
% the definite integral using sum(f.*w);
%
% Written by Greg von Winckel - 02/25/2004
N=N-1;
N1=N+1; N2=N+2;

xu=linspace(-1,1,N1)';

% Initial guess
y=cos((2*(0:N)'+1)*pi/(2*N+2))+(0.27/N1)*sin(pi*xu*N/N2);

% Legendre-Gauss Vandermonde Matrix
L=zeros(N1,N2);

% Derivative of LGVM
Lp=zeros(N1,N2);

% Compute the zeros of the N+1 Legendre Polynomial
% using the recursion relation and the Newton-Raphson method

y0=2;

% Iterate until new points are uniformly within epsilon of old points
while max(abs(y-y0))>eps
    
    
    L(:,1)=1;
    
    L(:,2)=y;
    
    for k=2:N1
        L(:,k+1)=( (2*k-1)*y.*L(:,k)-(k-1)*L(:,k-1) )/k;
    end
 
    Lp=(N2)*( L(:,N1)-y.*L(:,N2) )./(1-y.^2);   
    
    y0=y;
    y=y0-L(:,N2)./Lp;
    
end

% Linear map from[-1,1] to [a,b]
x=(a*(1-y)+b*(1+y))/2;      

% Compute the weights
w=(b-a)./((1-y.^2).*Lp.^2)*(N2/N1)^2;

end