clc;
clear;
close all;

% object id to plot
figId = 1;

% plot settings
set(0, 'defaultaxesfontsize', 24); set(0, 'defaulttextfontsize', 24);
set(0, 'defaultfigureposition', [1, 1, 1200, 900]);
set(0,'DefaultTextInterpreter', 'latex'); delete('*~');

% frozen files
defaultFrozenTargAvg   = "/Users/nreiland/Documents/codes/cookConstellation/output/frozenTargetAvg.txt";
defaultFrozenTargOsc   = "/Users/nreiland/Documents/codes/cookConstellation/output/frozenTargetOsc.txt";

% nominal files
defaultNominalTargAvg  = "/Users/nreiland/Documents/codes/cookConstellation/output/nominalTargetAvg.txt";
defualtNominalTargOsc  = "/Users/nreiland/Documents/codes/cookConstellation/output/nominalTargetOsc.txt";

% optimal files
defaultOptimalTargOsc  = "/Users/nreiland/Documents/codes/cookConstellation/output/optimalFrozenTargetOsc.txt";
defaultOptimalTargAvg  = "/Users/nreiland/Documents/codes/cookConstellation/output/optimalFrozenTargetAvg.txt";

% frozens
AvgFileFrozen  = defaultFrozenTargAvg;
OscFileFrozen  = defaultFrozenTargOsc;

% nominal
AvgFileNominal = defaultNominalTargAvg;
OscFileNominal = defualtNominalTargOsc;

% optimal
OscFileOptimal = defaultOptimalTargOsc;
AvgFileOptimal = defaultOptimalTargAvg;

% frozen Ics
avgIcsF = dlmread(AvgFileFrozen);
oscIcsF = dlmread(OscFileFrozen);

% nominal Ics
avgIcsN = dlmread(AvgFileNominal);
oscIcsN = dlmread(OscFileNominal);

% optimal Ics
oscIcsO = dlmread(OscFileOptimal);
avgIcsO = dlmread(AvgFileOptimal);

n = size(avgIcsF, 1);

% zonals
% C20  = -4.84165115570150018E-04;
% C30  = 9.58574916471960067E-07;
% C40  = 5.39787841725119966E-07;
% C50  = 6.72043988903809951E-08;
% C60  = -1.49852403801750000E-07;
% C70  = 9.31136691833619994E-08;
% C80  = 5.04645062929899971E-08;
% C90  = 2.62076310144059992E-08;
% C100 = 5.07619144933030026E-08;
% C110 = -5.02353915950460007E-08;
% C120 = 4.21820883377799989E-08;
% C130 = 4.22773579137330000E-08;
% C140 = -3.13506139224660021E-08;
% C150 = -2.85893335185249987E-11;
%  
% % // non-normalized
% J2  = -C20*sqrt(1 + 2 * 2);
% J3  = -C30*sqrt(1 + 3 * 2);
% J4  = -C40*sqrt(1 + 4 * 2);
% J5  = -C50*sqrt(1 + 5 * 2);
% J6  = -C60*sqrt(1 + 6 * 2);
% J7  = -C70*sqrt(1 + 7 * 2);
% J8  = -C80*sqrt(1 + 8 * 2);
% J9  = -C90*sqrt(1 + 9 * 2);
% J10 = -C100*sqrt(1 + 10 * 2);
% J11 = -C110*sqrt(1 + 11 * 2);
% J12 = -C120*sqrt(1 + 12 * 2);
% J13 = -C130*sqrt(1 + 13 * 2);
% J14 = -C140*sqrt(1 + 14 * 2);
% J15 = -C150*sqrt(1 + 15 * 2);

J2 = 0.10826261107488972E-02;
J3 = -0.25361508420093188E-05;
J4 = -0.16193635251753600E-05;
J5 = -0.22289177538077055E-06;
J6 = 0.54030052565874444E-06;
J7 = -0.36062769005142777E-06;
J8 = -0.20807048979064489E-06;
J9 = -0.11423641514139811E-06;
J10 = -0.23262031558643691E-06;
J11 = 0.24092047459748041E-06;
J12 = -0.21091044168889998E-06;
J13 = -0.21967959574907912E-06;
J14 = 0.16882822277732491E-06;
J15 = 0.15917867232150533E-09;

J4 = 0;
J6 = 0;
J7 = 0;
J8 = 0;
J9 = 0;
J10 = 0;
J11 = 0;
J12 = 0;
J13 = 0;
J14 = 0;
J15 = 0;
% Constants
% const.mu      = 3.986004415e5;          % [km^3/s^2] Earth's grav. par.
% const.R       = 6378.137;               % [km] Earth's equatorial radius
const.mu = 3.986004414498200E+05;
const.R = 6.378136460000000e3;
% const.J       = [  1.082635854e-3; ...
%                  - 2.532435346e-6; ...
%                  - 1.619331205e-6; ...
%                  - 2.277161016e-6; ...
%                    5.396484906e-7; ...
%                  - 3.513684422e-7; ...
%                  - 2.025187152e-7 ];    % [-] Harmonics [J2, J3, J4, ...]
% const.J       = [  1.082635854e-3; ...
%                  - 2.532435346e-6; ...
%                  0; ...
%                  - 2.277161016e-6; ...
%                    0; ...
%                  - 3.513684422e-7; ...
%                  0 ];    % [-] Harmonics [J2, J3, J4, ...]
const.J = [J2;J3;J4;J5;J6;J7;J8;J9;J10:J11;J12:J13;J14;J15]; 
% Integration of mean and osculating trajectories
tspan = 86400*6;
t             = linspace(0, tspan, 1000);  % [s] Integrate for 1 day

odeopts       = odeset('reltol', 1e-12, 'abstol', 1e-12);

N = length(t);

% preallocate
oeOscF = cell(n,1);
oeAvgF = cell(n,1);

oeOscN = cell(n,1);
oeAvgN = cell(n,1);

oeOscO = cell(n,1);
oeAvgO = cell(n,1);

devN = zeros(n,6);
devF = zeros(n,6);
devO = zeros(n,6);

for i = 1:n
    oeOscF{i} = zeros(N,6);
    oeAvgF{i} = zeros(N,6);
    
    oeOscN{i} = zeros(N,6);
    oeAvgN{i} = zeros(N,6);
    
    oeOscO{i} = zeros(N,6);
    oeAvgO{i} = zeros(N,6);
end

for i = 1:1
    avg0F = avgIcsF(i, 2:7);
    osc0F = oscIcsF(i, 2:7);
    
    avg0N = avgIcsN(i, 2:7);
    osc0N = oscIcsN(i, 2:7);
    
    avg0O = avgIcsO(i, 2:7);
    osc0O = oscIcsO(i, 2:7);
    
    oeOscF{i} = dynamics_zonal(t, osc0F, const, 'osc', 'deg', odeopts);
    oeAvgF{i} = dynamics_zonal(t, avg0F, const, 'mean', 'deg', odeopts);
    
    oeOscN{i} = dynamics_zonal(t, osc0N, const, 'osc', 'deg', odeopts);
    oeAvgN{i} = dynamics_zonal(t, avg0N, const, 'mean', 'deg', odeopts);
    
    oeOscO{i} = dynamics_zonal(t, osc0O, const, 'osc', 'deg', odeopts);
    oeAvgO{i} = dynamics_zonal(t, avg0O, const, 'mean', 'deg', odeopts);
    
    for j = 1:6
    
        devN(i,j) = var(oeAvgN{i}(j,:));
        devF(i,j) = var(oeAvgF{i}(j,:));
        devO(i,j) = var(oeAvgO{i}(j,:));
        
    end
    
    
end

% compute rms standard deviation
rmsN = rms(devN);
rmsF = rms(devF);
rmsO = rms(devO);

%% Plot of frozen trajectories

h1 = figure(1);

% figure output
name = 'frozenTest.pdf';

subplot(3, 2, 1); hold on; box on; grid off;
plot(t / 3600, oeOscF{figId}(1, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgF{figId}(1, :), 'b', 'linewidth', 2);  hold on
xlabel('t [h]'); ylabel('a [km]');

subplot(3, 2, 2); hold on; box on; grid off;
plot(t / 3600, oeOscF{figId}(2, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgF{figId}(2, :), 'b', 'linewidth', 2);  hold on
ylabel('e [-]');

subplot(3, 2, 3); hold on; box on; grid off;
plot(t / 3600, oeOscF{figId}(3, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgF{figId}(3, :), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$i$ [deg]');

subplot(3, 2, 4); hold on; box on; grid off;
plot(t / 3600, oeOscF{figId}(4, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgF{figId}(4, :), 'b', 'linewidth', 2);  hold on
ylabel('$\Omega$ [deg]');

subplot(3, 2, 5); hold on; box on; grid off;
plot(t / 3600, oeOscF{figId}(5, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgF{figId}(5, :), 'b', 'linewidth', 2);  hold on
ylabel('$\omega$ [deg]');

subplot(3, 2, 6); hold on; box on; grid off;
plot(t / 3600, oeOscF{figId}(6, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgF{figId}(6, :), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$M$ [deg]');

% save fig
h1.PaperSize = [ 17 27];
print(h1, '-dpdf', '-fillpage', name);

%% Plot of default trajectories

% figure output
name = 'nominalTest.pdf';

h2 = figure(2);

subplot(3, 2, 1); hold on; box on; grid off;
plot(t / 3600, oeOscN{figId}(1, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgN{figId}(1, :), 'b', 'linewidth', 2);  hold on
xlabel('t [h]'); ylabel('a [km]');

subplot(3, 2, 2); hold on; box on; grid off;
plot(t / 3600, oeOscN{figId}(2, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgN{figId}(2, :), 'b', 'linewidth', 2);  hold on
ylabel('e [-]');

subplot(3, 2, 3); hold on; box on; grid off;
plot(t / 3600, oeOscN{figId}(3, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgN{figId}(3, :), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$i$ [deg]');

subplot(3, 2, 4); hold on; box on; grid off;
plot(t / 3600, oeOscN{figId}(4, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgN{figId}(4, :), 'b', 'linewidth', 2);  hold on
ylabel('$\Omega$ [deg]');

subplot(3, 2, 5); hold on; box on; grid off;
plot(t / 3600, oeOscN{figId}(5, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgN{figId}(5, :), 'b', 'linewidth', 2);  hold on
ylabel('$\omega$ [deg]');

subplot(3, 2, 6); hold on; box on; grid off;
plot(t / 3600, oeOscN{figId}(6, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgN{figId}(6, :), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$M$ [deg]');

% save fig
h2.PaperSize = [ 17 27];
print(h2, '-dpdf', '-fillpage', name);

%% Plot of optimal trajectories

% figure output
name = 'optimalTest.pdf';

h3 = figure(3);

subplot(3, 2, 1); hold on; box on; grid off;
plot(t / 3600, oeOscO{figId}(1, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgO{figId}(1, :), 'b', 'linewidth', 2);  hold on
xlabel('t [h]'); ylabel('a [km]');

subplot(3, 2, 2); hold on; box on; grid off;
plot(t / 3600, oeOscO{figId}(2, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgO{figId}(2, :), 'b', 'linewidth', 2);  hold on
ylabel('e [-]');

subplot(3, 2, 3); hold on; box on; grid off;
plot(t / 3600, oeOscO{figId}(3, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgO{figId}(3, :), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$i$ [deg]');

subplot(3, 2, 4); hold on; box on; grid off;
plot(t / 3600, oeOscO{figId}(4, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgO{figId}(4, :), 'b', 'linewidth', 2);  hold on
ylabel('$\Omega$ [deg]');

subplot(3, 2, 5); hold on; box on; grid off;
plot(t / 3600, oeOscO{figId}(5, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgO{figId}(5, :), 'b', 'linewidth', 2);  hold on
ylabel('$\omega$ [deg]');

subplot(3, 2, 6); hold on; box on; grid off;
plot(t / 3600, oeOscO{figId}(6, :), 'r', 'linewidth', 2);  hold on
plot(t / 3600, oeAvgO{figId}(6, :), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$M$ [deg]');

% save fig
h3.PaperSize = [ 17 27];
print(h3, '-dpdf', '-fillpage', name);


%% Plot of frozen trajectories

h4 = figure(4);

% figure output
name = 'frozenTestAvg.pdf';

subplot(3, 2, 1); hold on; box on; grid off;
plot(t / 3600, oeAvgF{figId}(1, :) - oeAvgF{figId}(1, 1), 'b', 'linewidth', 2);  hold on
xlabel('t [h]'); ylabel('a [km]');

subplot(3, 2, 2); hold on; box on; grid off;
plot(t / 3600, oeAvgF{figId}(2, :) - oeAvgF{figId}(2, 1), 'b', 'linewidth', 2);  hold on
ylabel('e [-]');

subplot(3, 2, 3); hold on; box on; grid off;
plot(t / 3600, oeAvgF{figId}(3, :) - oeAvgF{figId}(3, 1), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$i$ [deg]');

subplot(3, 2, 4); hold on; box on; grid off;
plot(t / 3600, oeAvgF{figId}(4, :) - oeAvgF{figId}(4, 1), 'b', 'linewidth', 2);  hold on
ylabel('$\Omega$ [deg]');

subplot(3, 2, 5); hold on; box on; grid off;
plot(t / 3600, oeAvgF{figId}(5, :) - oeAvgF{figId}(5, 1), 'b', 'linewidth', 2);  hold on
ylabel('$\omega$ [deg]');

subplot(3, 2, 6); hold on; box on; grid off;
plot(t / 3600, oeAvgF{figId}(6, :) - oeAvgF{figId}(6, 1), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$M$ [deg]');

% save fig
h4.PaperSize = [ 17 27];
print(h4, '-dpdf', '-fillpage', name);

%% Plot of default trajectories

% figure output
name = 'nominalTestAvg.pdf';

h5 = figure(5);

subplot(3, 2, 1); hold on; box on; grid off;
plot(t / 3600, oeAvgN{figId}(1, :) - oeAvgN{figId}(1, 1), 'b', 'linewidth', 2);  hold on
xlabel('t [h]'); ylabel('a [km]');

subplot(3, 2, 2); hold on; box on; grid off;
plot(t / 3600, oeAvgN{figId}(2, :) - oeAvgN{figId}(2, 1), 'b', 'linewidth', 2);  hold on
ylabel('e [-]');

subplot(3, 2, 3); hold on; box on; grid off;
plot(t / 3600, oeAvgN{figId}(3, :) - oeAvgN{figId}(3, 1), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$i$ [deg]');

subplot(3, 2, 4); hold on; box on; grid off;
plot(t / 3600, oeAvgN{figId}(4, :) - oeAvgN{figId}(4, 1), 'b', 'linewidth', 2);  hold on
ylabel('$\Omega$ [deg]');

subplot(3, 2, 5); hold on; box on; grid off;
plot(t / 3600, oeAvgN{figId}(5, :) - oeAvgN{figId}(5, 1), 'b', 'linewidth', 2);  hold on
ylabel('$\omega$ [deg]');

subplot(3, 2, 6); hold on; box on; grid off;
plot(t / 3600, oeAvgN{figId}(6, :) - oeAvgN{figId}(6, 1), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$M$ [deg]');

% save fig
h5.PaperSize = [ 17 27];
print(h5, '-dpdf', '-fillpage', name);

%% Plot of optimal trajectories

% figure output
name = 'optimalTestAvg.pdf';

h6 = figure(6);

subplot(3, 2, 1); hold on; box on; grid off;
plot(t / 3600, oeAvgO{figId}(1, :) - oeAvgO{figId}(1, 1), 'b', 'linewidth', 2);  hold on
xlabel('t [h]'); ylabel('a [km]');

subplot(3, 2, 2); hold on; box on; grid off;
plot(t / 3600, oeAvgO{figId}(2, :) - oeAvgO{figId}(2, 1), 'b', 'linewidth', 2);  hold on
ylabel('e [-]');

subplot(3, 2, 3); hold on; box on; grid off;
plot(t / 3600, oeAvgO{figId}(3, :) - oeAvgO{figId}(3, 1), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$i$ [deg]');

subplot(3, 2, 4); hold on; box on; grid off;
plot(t / 3600, oeAvgO{figId}(4, :) - oeAvgO{figId}(4, 1), 'b', 'linewidth', 2);  hold on
ylabel('$\Omega$ [deg]');

subplot(3, 2, 5); hold on; box on; grid off;
plot(t / 3600, oeAvgO{figId}(5, :) - oeAvgO{figId}(5, 1), 'b', 'linewidth', 2);  hold on
ylabel('$\omega$ [deg]');

subplot(3, 2, 6); hold on; box on; grid off;
plot(t / 3600, oeAvgO{figId}(6, :) - oeAvgO{figId}(6, 1), 'b', 'linewidth', 2);  hold on
xlabel('$t$ [h]'); ylabel('$M$ [deg]');

% save fig
h6.PaperSize = [ 17 27];
print(h6, '-dpdf', '-fillpage', name);

%% plot satellites
name2 = 'frozenVsNominalTarget.pdf';

plotSets1{1} = defaultOptimalTargOsc;
plotSets1{2} = defualtNominalTargOsc;
colors1{1} = 'red';
colors1{2} = 'blue';
h2 = plotSats(plotSets1, colors1);

h2.PaperSize = [ 17 27];
print(h2, '-dpdf', '-fillpage', name2);

