// physical constants

// Seconds per milisecond
#define S2MS (1000.0)

// degrees to radians
#define d2r (M_PI/180.0)

// radians to degrees
#define r2d (180.0/M_PI)

// rotations rate of the earth from Vallado
#define OMEGAEARTH (7.292115146706979e-5)

// accelleration due to J2
// #define J2 (0.001082626110749)

// radius of Earth in km
#define REarth (6.378136460000000e3)

// Earth standard gravitational parameter
#define MU (3.986004414498200E+05)

// max line length
#define MAXLINE 1000