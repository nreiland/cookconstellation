#include "./kissfft/kiss_fft.h"

// function to allocate heap memory for a 2d array
double** allocDouble(int rows, int columns, double *mem)
{
    double **arr = (double **)malloc(rows * sizeof(double *));
    (*mem) = rows*sizeof(double *); 
    for (int i=0; i<rows; i++) {
        arr[i] = (double *)malloc(columns * sizeof(double));
        (*mem) += columns*sizeof(double);
    }

    return arr; 
}

/*function to allocate and array of pointers to rows 1D array (non contiguous)*/
double** allocateRowArray(int rows, int columns, double *mem)
{
    double **arr = (double **)malloc(rows * sizeof(double *));
    (*mem) = rows*sizeof(double *); 
    for (int i=0; i<rows; i++) {
        arr[i] = (double *)malloc(columns * sizeof(double));
        (*mem) += columns*sizeof(double);
    }

    return arr; 
}

// function to copy elements from a 3x3 stack array to a 3x3 heap array
void stack3heap3(double stackArr[3][3], double **heapArr) {
    int i;
    int j;

    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            heapArr[i][j] = stackArr[i][j];
        }     
    }
}

// function to print elements of a heap array
void printHeapArr(double **heapArr, int rows, int cols) {
    int i;
    int j;

    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            if (j == 0) {
                printf("| %.15f, ", heapArr[i][j]);
            }
            else if (j == cols - 1) {
                printf("%.15f | \n", heapArr[i][j]);
            }
            else {
                printf("%.15f, ", heapArr[i][j]);
            }
        }     
    }
}

// function to print elements of a 3x3 stack array
void printStackArr3by3(double heapArr[3][3]) {
    int i;
    int j;
    int rows = 3;
    int cols = 3;

    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            if (j == 0) {
                printf("| %.15f, ", heapArr[i][j]);
            }
            else if (j == cols - 1) {
                printf("%.15f | \n", heapArr[i][j]);
            }
            else {
                printf("%.15f, ", heapArr[i][j]);
            }
        }     
    }
}

// function to print elements of a nx3 stack array
void printStackArr_nx3(double stackArr[][3], int rows) {
    int i;
    int j;
    int cols = 3;

    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            if (j == 0) {
                printf("%d -> | %.15f, ", i + 1, stackArr[i][j]);
            }
            else if (j == cols - 1) {
                printf("%d -> %.15f | \n", i + 1, stackArr[i][j]);
            }
            else {
                printf("%d -> %.15f, ", i + 1, stackArr[i][j]);
            }
        }     
    }
}

// function to print elements of a nx3 stack array
void printStackArr_nx10(double stackArr[][10], int rows) {
    int i;
    int j;
    int cols = 10;

    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            if (j == 0) {
                printf("%d -> | %.15f, ", i + 1, stackArr[i][j]);
            }
            else if (j == cols - 1) {
                printf("%d -> %.15f | \n", i + 1, stackArr[i][j]);
            }
            else {
                printf("%d -> %.15f, ", i + 1, stackArr[i][j]);
            }
        }     
    }
}

// function to print elements of a heap vector
void printHeapVec(double *heapArr, int el) {
    int i;
    int j;

    for (i = 0; i < el; i++) {
        if (i== el - 1) {
            printf("%d -> %.15f\n ", i, heapArr[i]);
        }
        else {
            printf("%d -> %.15f\n ", i, heapArr[i]);
        }
    }
}

// function to print elements of a stack vector
void printStackVec(double stackArr[], int el) {
    int i;
    int j;

    for (i = 0; i < el; i++) {
        if (i== el - 1) {
            printf("%d -> %.25f\n ", i + 1, stackArr[i]);
        }
        else {
            printf("%d -> %.25f\n ", i + 1, stackArr[i]);
        }
    }
}

// function to print elements of a comlex stack vector
void printComplexStackVec(kiss_fft_cpx stackArr[], int el) {
    int i;
    int j;

    for (i = 0; i < el; i++) {
        if (i== el - 1) {
            printf("%d -> %.25f + %.25fi\n ", i + 1, (double)stackArr[i].r, (double)stackArr[i].i);
        }
        else {
            printf("%d -> %.25f + %.25fi\n ", i + 1, (double)stackArr[i].r, (double)stackArr[i].i);
        }
    }
}

// function to compute norm of a 3d vector
double normvec(double x[3]) {
    double norm;
    norm = sqrt(x[0]*x[0] + x[1]*x[1] + x[2]*x[2]);
    return norm;
}

// function to compute norm of the components 3d vector
double normcompvec(double x, double y, double z) {
    double norm;
    norm = sqrt(x*x + y*y + z*z);
    return norm;
}

// function to comput the outer prodcut of two 3d vectors
void outerProduct(double u[3], double v[3], double A[3][3]) {

    double u1 = u[0];
    double u2 = u[1];
    double u3 = u[2];
    double v1 = v[0];
    double v2 = v[1];
    double v3 = v[2];

    A[0][0] = u1*v1;
    A[0][1] = u1*v2;
    A[0][2] = u1*v3;
    A[1][0] = u2*v1;
    A[1][1] = u2*v2;
    A[1][2] = u2*v3;
    A[2][0] = u3*v1;
    A[2][1] = u3*v2;
    A[2][2] = u3*v3;
}

// function to multiply a 1x3 by 3x3 array
void revRxp(double u[3], double A[3][3], double v[3]) {

    double u1 = u[0];
    double u2 = u[1];
    double u3 = u[2];

    double a11 = A[0][0];
    double a12 = A[0][1];
    double a13 = A[0][2];
    double a21 = A[1][0];
    double a22 = A[1][1];
    double a23 = A[1][2];
    double a31 = A[2][0];
    double a32 = A[2][1];
    double a33 = A[2][2];

    v[0] = a11*u1 + a21*u2 + a31*u3;
    v[1] = a12*u1 + a22*u2 + a32*u3;
    v[2] = a13*u1 + a23*u2 + a33*u3;
    printf("test:");
    printStackVec(v, 3);   

}

// function to multiply a 3x3 array by a constant
void cxR(double c, double A[3][3], double B[3][3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            B[i][j] = c*A[i][j];
        }
    }
}

// function to add two 3x3 arrays
void RpR(double A[3][3], double B[3][3], double C[3][3]) {
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            C[i][j] = A[i][j] + B[i][j];
        }
    }
}

// function to convert kiss 

// function to circular shift a complex kiss array to the right by one
void rotateByOne(kiss_fft_cpx arr[], int length) {
    int i;
    kiss_fft_cpx last;

    // Store last element of array
    last = arr[length - 1];

    for (i = length - 1; i > 0; i--) {

        // Move each array element to its right
        arr[i] = arr[i - 1];
    }

    // Copy last element of array to first
    arr[0] = last;
}

// function to circular shift a complex kiss vector
void kissCircShift(kiss_fft_cpx arr[], int length, int N) {

    // interators
    int i;

    /* Rotate array n times */
    for (i = 0; i < N; i++) {
        rotateByOne(arr, length);
    }

}

// function to copy a kiss array to a complex array
void kiss2Complex(kiss_fft_cpx arrIn[], complex arrOut[], int length) {

    int i;;

    /* Rotate array n times */
    for (i = 0; i < length; i++) {
        arrOut[i].r = arrIn[i].r;
        arrOut[i].i = arrIn[i].i;
    }
}

// function to multiply a complex kiss vector by -1i
void kissFlip(kiss_fft_cpx arr[], int length) {

    int i;
    kiss_fft_cpx temp;

    /* Rotate array n times */
    for (i = 0; i < length; i++) {
        temp.r = arr[i].i;
        temp.i = arr[i].r * -1.0;
        arr[i].r = temp.r;
        arr[i].i = temp.i;
    }

}

// function to multiply a complex kiss vector by -1i
void complexFlip(complex arr[], int length) {

    int i;
    complex temp;

    /* Rotate array n times */
    for (i = 0; i < length; i++) {
        temp.r = arr[i].i;
        temp.i = arr[i].r * -1.0;
        arr[i].r = temp.r;
        arr[i].i = temp.i;
    }

}

// convert exp(iX) array to complex number array using Euler's Equation
void eulerEqArr(double X[], int length, complex expix[]) {
    
    int i;
    for (i = 0; i < length; i++) {
        expix[i].r = cos(X[i]);
        expix[i].i = sin(X[i]);
    }
}


// function to print elements of a comlex stack vector
void printCStackVec(complex stackArr[], int el) {
    int i;
    int j;

    for (i = 0; i < el; i++) {
        if (i== el - 1) {
            printf("%d -> %.25f + %.25fi\n ", i + 1, stackArr[i].r, stackArr[i].i);
        }
        else {
            printf("%d -> %.25f + %.25fi\n ", i + 1, stackArr[i].r, stackArr[i].i);
        }
    }
}

// function to print elements of a complex nx6 stack array
void printComplexStackArr_nx6(complex stackArr[][6], int rows) {
    int i;
    int j;
    int cols = 6;

    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            if (j == 0) {
                printf("%d -> | %.25f + %.25fi, ", i + 1, stackArr[i][j].r, stackArr[i][j].i);
            }
            else if (j == cols - 1) {
                printf("%d -> %.25f + %.25fi| \n", i + 1, stackArr[i][j].r, stackArr[i][j].i);
            }
            else {
                printf("%d -> %.25f + %.25fi, ", i + 1, stackArr[i][j].r, stackArr[i][j].i);
            }
        }     
    }
}

void sumComplexStackArr_nx6(complex stackArr[][6], int rows) {
    int i;
    int j;
    int cols = 6;
    complex sum[cols];

    sum[0].r = 0;
    sum[1].r = 0;
    sum[2].r = 0;
    sum[3].r = 0;
    sum[4].r = 0;
    sum[5].r = 0;
    sum[0].i = 0;
    sum[1].i = 0;
    sum[2].i = 0;
    sum[3].i = 0;
    sum[4].i = 0;
    sum[5].i = 0;

    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            sum[j].r = sum[j].r + stackArr[i][j].r;
            sum[j].i = sum[j].i + stackArr[i][j].i;
        }     
    }

    printCStackVec(sum, cols);
}

// helper function to compute normc of array
void normc(double A[][3], double B[][3], int n ) {

    double nval;

    int i,j;

    for (j = 0; j < n; j++) {
        nval = normcompvec(A[j][0], A[j][1], A[j][2]);
        for (i = 0; i < 3; i++) {
            B[j][i] = A[j][i]/nval;
        }
    }

}