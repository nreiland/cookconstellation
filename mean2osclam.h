#include <stdio.h>
#include <math.h>
#include "constants.h"
#include "./SOFA/sofa.h"
#include "./kissfft/kiss_fft.h"

// (keplerequin l ex ey n L) < Helper function to transform mean anomaly 
//  to true longitude. As inputs the function takes an array, l, a double
//  ex, a double dy, an integer n, which is the length of l, and returns
//  an array L, which is the true longitude and has the same lengthe as
//  l. >
void keplerequin(double l[], double ex, double ey, int n, double L[]) {

    int i;
    double eta;
    double AA;
    double BB;
    double CC;
    double K;
    double cK;
    double sK;
    double f;
    double df;
    double tol = 1e-13;
    double sL;
    double cL;

    for (i = 0; i < n; i++) {

        eta = sqrt(1.0 - ex*ex - ey*ey);
        AA  = 1.0 - (ex*ex) / (1.0 + eta);
        BB  = ex*ey / (1 + eta);
        CC  = 1.0 - ey*ey / (1.0 + eta);
        K   = l[i];
        cK  = cos(K);
        sK  = sin(K);
        f   = 1.0;

        while (fabs(f) > tol) {
            f  = K + ey * cK - ex *sK - l[i];
            df = 1.0 - ey * sK - ex * cK;
            K  = K - f / df;
            cK = cos(K);
            sK = sin(K);
        }

        sL = (AA * sK + BB * cK - ey) / (1.0 - ey * sK - ex * cK);
        cL = (BB * sK + CC * cK - ex) / (1.0 - ey * sK - ex * cK);
        L[i] = atan2(sL, cL);

    }

}

// (grav rECEF Ji n N FG) < This helper function calculate the gravitational 
// force on an Earth-orbiting satellite due to the zonal harmonics of the 
// Earth's geopotential. As inputs the function takes the position of the 
// satellite in ECI frame in the form of an n x 3 array, rECEF, an array of 
// the coefficients of the geopotential, Ji, the length of rECEF, n, the 
// length of Ji, N, and returns the n x 3 array, Fg, which is the components
// of the force on the satellite.
void grav(double rECEF[][3], double Ji[], int n, int N, double Fg[][3]) {

    // iteration variables
    int i, j;
    double k;

    // position vector
    double x[n];
    double y[n];
    double z[n];

    // magnitude of position vector and other nD constants;
    double r[n];
    double r2[n];
    double Re_r2[n];
    double x_Re_r2[n];
    double y_Re_r2[n];
    double z_Re_r2[n];
    double Re2_r2[n];

    // 1D constants
    double Re2 = REarth*REarth;
    double mu_Re2 = MU / Re2;

    // populate xyz and r and nD constants
    for (i = 0; i < n; i ++) {
        x[i]       = rECEF[i][0];
        y[i]       = rECEF[i][1];
        z[i]       = rECEF[i][2];
        r[i]       = normcompvec(x[i], y[i], z[i]); 
        r2[i]      = r[i]*r[i];
        Re_r2[i]   = REarth / r2[i];
        x_Re_r2[i] = x[i] * Re_r2[i];
        y_Re_r2[i] = y[i] * Re_r2[i];
        z_Re_r2[i] = z[i] * Re_r2[i];
        Re2_r2[i]  = Re2 / r2[i];
    }

    // printf("r:\n");
    // printStackVec(r, n);

    // Fg computation
    double Vz[n][N + 2];
    for (i = 0; i < n; i++) {
        Vz[i][0] = REarth/r[i];
        Vz[i][1] = z_Re_r2[i] * Vz[i][0];
    }
    for (j = 1; j < N + 1; j++) {
        for (i = 0; i < n; i ++) {
            k = j + 1.0;
            Vz[i][j + 1] = ((2.0 * k - 1.0) * z_Re_r2[i] * Vz[i][j] - (k - 1.0) * Re2_r2[i] * Vz[i][j - 1]) / k;
        }
    }

    // printf("Vz:\n");
    // printStackArr_nx10(Vz, n);

    // allocation
    double Vx[n][N + 2];
    double Vy[n][N + 2];

    // first columns
    for (i = 0; i < n; i ++) {
        Vx[i][1] = x_Re_r2[i] * Vz[i][0];
        Vy[i][1] = y_Re_r2[i] * Vz[i][0];
        Vx[i][2] = 3.0 * z_Re_r2[i] * Vx[i][1];
        Vy[i][2] = 3.0 * z_Re_r2[i] * Vy[i][1];
    }

    // remaining columns
     for (j = 2; j < N + 1; j++) {
        for (i = 0; i < n; i ++) {
            k = j + 1.0;
            Vx[i][j + 1] = ((2.0 * k - 1.0) * z_Re_r2[i] * Vx[i][j] - k * Re2_r2[i] * Vx[i][j - 1]) / (k - 1.0);
            Vy[i][j + 1] = ((2.0 * k - 1.0) * z_Re_r2[i] * Vy[i][j] - k * Re2_r2[i] * Vy[i][j - 1]) / (k - 1.0);
        }
    }

    // printf("Vy:\n");
    // printStackArr_nx10(Vy, n);

    // Evaluating the force
    double Fmat1[n];
    double Fmat2[n];
    double Fmat3[n];
    for (i = 0; i < n; i++) {
        Fmat1[i] = 0.0;
        Fmat2[i] = 0.0;
        Fmat3[i] = 0.0;
        for (j = 0; j < N; j ++) {
            k = j + 2.0;
            Fmat1[i] = Fmat1[i] + Ji[j] * Vx[i][j + 2];
            Fmat2[i] = Fmat2[i] + Ji[j] * Vy[i][j + 2];
            Fmat3[i] = Fmat3[i] + (Ji[j] * k) * Vz[i][j + 2];
        }

        // add to output
        Fg[i][0] = mu_Re2 * Fmat1[i]; 
        Fg[i][1] = mu_Re2 * Fmat2[i]; 
        Fg[i][2] = mu_Re2 * Fmat3[i]; 
    }

    // printf("Ji - %d\n", N);
    // printStackVec(Ji, N);

    // printf("Fmat1\n");
    // printStackVec(Fmat1, n); 

}

// mean2osc_zonal( coe, Ji, type, coep ) < First-order mapping (based on the theory 
// developed by Kozai, but using the Milankovitch variables) between mean 
// (orbit averaged, with short- and long-period motion removed) orbit elements 
// and osculating (instantaneous) orbit elements. The function Takes as inputs, an
// array of the the six classical(Keplerian) orbital elements, either mean or osculating
// with angles in radians and SMA in kilometers, an integer, nquads, representing the
// number of desired knots on the FFT, an array of zonal harmonics, Ji, an immutable 
// character, type, which is either 'o' or 'm' for osculating or mean. The function 
// returns coep, which contains the same orbital elements transformed to the type 
// opposite of coe (either mean->osc or osc->mean).>
void mean2osc_zonal(double coe[6], int nquads, double Ji[], int nis, const char type, double coep[6]) {

    // iterators
    int i, j;

    // classical orbital elements
    double a  = coe[0];
    double e  = coe[1];
    double in = coe[2];
    double Om = coe[3];
    double w  = coe[4];
    double M  = coe[5];

    // equinoctial elements
    double coeeq[6];
    double ti = tan(in / 2.0);
    double p  = a * (1 - e * e);
    double P1 = e * sin(w + Om);
    double P2 = e * cos(w + Om);
    double Q1 = ti * sin(Om);
    double Q2 = ti * cos(Om);
    double l  = M + w + Om;
    coeeq[0] = a;
    coeeq[1] = P1;
    coeeq[2] = P2;
    coeeq[3] = Q1;
    coeeq[4] = Q2;
    coeeq[5] = l;

    // mean motion
    double n = sqrt(MU / (a * a * a));

    // partial derivative of n wrt to coeeq
    double dndx[6];
    dndx[0] = -3.0 / 2.0 * n / a;
    dndx[1] = 0;
    dndx[2] = 0;
    dndx[3] = 0;
    dndx[4] = 0;    
    dndx[5] = 0;

    // knots of the FFT
    double dubquads = nquads;
 
    if (nquads == 0) {
        nquads = 361;
    }
    else if ((nquads / 2) == 0) {
        printf("Atttention! Number of points on FFT (%f) is even! Adding 1 point.\n", dubquads);
        nquads = nquads + 1;
    }

    // weights
    double idxX[nquads];
    double wComb[nquads];
    double lFT[nquads];
    double L[nquads];
    int i0;

    for (i = 0; i < nquads; i++) {
        idxX[i] = (i + 1) - ceil(nquads / 2.0);
        wComb[i] = idxX[i] * n;
    }
    
    i0 = dubquads / 2.0 + 0.5;

    // printStackVec(wComb, nquads);

    i0 = nquads / 2.0 + 0.5;

    double slope = (2.0 * M_PI) / (nquads);
    for (i = 0; i < nquads; i++) {
        lFT[i] = i * slope;
    }

    // printf("LFT\n");
    // printStackVec(lFT, nquads);

    // obtain true longitude from Kepler's equation
    keplerequin(lFT, P2, P1, nquads, L);

    // printf("P2: %f, P1: %f \n", P2, P1);
    // printf("L\n");
    // printStackVec(L, nquads);

    // LVLH frame and other useful variables
    double sL[nquads];
    double cL[nquads];
    double b;
    double h;
    double por[nquads];
    double r[nquads];
    double roh[nquads];
    double Q22;
    double Q11;
    double Q12;
    double eta;
    double rDir[nquads][3];
    double R[nquads][3];
    double VxPFdir[nquads];
    double VyPFdir[nquads];
    double vDir[nquads][3];
    double hDir[nquads][3];
    double tDir[nquads][3];
    double ri[3], vi[3], hi[3], tii[3], normval;

    for (i = 0; i < nquads; i++) {
        sL[i]      = sin(L[i]);
        cL[i]      = cos(L[i]);
        b          = a * sqrt(1.0  - P1 * P1 - P2 * P2);
        h          = sqrt(MU * p);
        por[i]     = 1.0 + P1 * sL[i] + P2 * cL[i];
        r[i]       = p / (1.0 + P1 * sL[i] + P2 * cL[i]);
        roh[i]     = r[i] / h;
        Q22        = Q2 * Q2;
        Q11        = Q1 * Q1;
        Q12        = 2.0 * Q1 * Q2;
        eta        = 1.0 + Q22 + Q11;
        rDir[i][0] = ((1.0 - Q11 + Q22) * cL[i] + Q12 * sL[i]) / eta;
        rDir[i][1] = ((1.0 + Q11 - Q22) * sL[i] + Q12 * cL[i]) / eta;
        rDir[i][2] = 2.0 * (-1.0 * Q1 * cL[i] + Q2 * sL[i]) / eta;
        R[i][0]    = rDir[i][0] * r[i];
        R[i][1]    = rDir[i][1] * r[i];
        R[i][2]    = rDir[i][2] * r[i];
        VxPFdir[i] = -1.0 * (P1 * sL[i]);
        VyPFdir[i] = (P1 + cL[i]);
        vDir[i][0] = ((1.0 - Q11 + Q22) * VxPFdir[i] + Q12 * VyPFdir[i]) / eta;
        vDir[i][1] = ((1.0 + Q11 - Q22) * VyPFdir[i] + Q12 * VxPFdir[i]) / eta;
        vDir[i][2] = 2.0 * (-1.0 * Q1 * VxPFdir[i] + Q2 * VyPFdir[i]) / eta;
        ri[0]      = rDir[i][0];
        ri[1]      = rDir[i][1];
        ri[2]      = rDir[i][2];
        vi[0]      = vDir[i][0];
        vi[1]      = vDir[i][1];
        vi[2]      = vDir[i][2];
        iauPxp(ri, vi, hi);
        normval    = normvec(hi);
        hDir[i][0] = hi[0]/normval;
        hDir[i][1] = hi[1]/normval;
        hDir[i][2] = hi[2]/normval;
        hi[0]      = hDir[i][0];
        hi[1]      = hDir[i][1];
        hi[2]      = hDir[i][2];
        iauPxp(hi, ri, tii);
        tDir[i][0] = tii[0];
        tDir[i][1] = tii[1];
        tDir[i][2] = tii[2];
    }

    // printf("cL\n");
    // printStackVec(cL, nquads);

    // Force components in the tangential-norm frame
    double Fg[nquads][3];
    double Fr[nquads];
    double Ft[nquads];
    double Fh[nquads];

    // compute force

    // printf("R: \n");
    // printStackArr_nx3(R, nquads);

    grav(R, Ji, nquads, nis, Fg);

    // printf("Fg: \n");
    // printStackArr_nx3(Fg, nquads);

    // compute sums
    for (i = 0; i < nquads; i++) {
        Fr[i] = rDir[i][0] * Fg[i][0] + rDir[i][1] * Fg[i][1] + rDir[i][2] * Fg[i][2];
        Ft[i] = tDir[i][0] * Fg[i][0] + tDir[i][1] * Fg[i][1] + tDir[i][2] * Fg[i][2];
        Fh[i] = hDir[i][0] * Fg[i][0] + hDir[i][1] * Fg[i][1] + hDir[i][2] * Fg[i][2];
    }

    // Gauss Variational Equations
    double dadt[nquads];
    double dP1dt[nquads];
    double dP2dt[nquads];
    double dQ1dt[nquads];
    double dQ2dt[nquads];
    double dldt[nquads];
    kiss_fft_cpx dcoeFt1[nquads], dcoeFt2[nquads], dcoeFt3[nquads];
    kiss_fft_cpx dcoeFt4[nquads], dcoeFt5[nquads], dcoeFt6[nquads];
    kiss_fft_cpx coutfFT1[nquads], coutfFT2[nquads], coutfFT3[nquads];
    kiss_fft_cpx coutfFT4[nquads], coutfFT5[nquads], coutfFT6[nquads];

    for (i = 0; i < nquads; i++) {

        // EOM
        dadt[i]  = 2.0 * a * a * ((P2 * sL[i] - P1 * cL[i]) * Fr[i] + por[i] * Ft[i]) / h;
        dP1dt[i] = roh[i] * (-1.0 * por[i] * cL[i] * Fr[i] + (P1 + (1.0 + por[i]) * sL[i]) * \
                    Ft[i] - P2 * (Q1 * cL[i] - Q2 * sL[i]) * Fh[i]);
        dP2dt[i] = roh[i] * (por[i] * sL[i] * Fr[i] + (P2 + (1.0 + por[i]) * cL[i]) * Ft[i] + \
                    P1 * (Q1 * cL[i] - Q2 * sL[i]) * Fh[i]);
        dQ1dt[i] = 0.5 * roh[i] * (1.0 + Q1 * Q1 + Q2 * Q2) * sL[i] * Fh[i];
        dQ2dt[i] = 0.5 * roh[i] * (1.0 + Q1 * Q1 + Q2 * Q2) * cL[i] * Fh[i];
        dldt[i]  = n - roh[i] * ((a * por[i] * (P1 * sL[i] + P2 * cL[i]) / (a + b) + 2.0 * \
                    b / a) * Fr[i] + a * (1 + por[i]) * (P1 * cL[i] - P2 * sL[i]) * Ft[i] / \
                    (a + b) + (Q1 * cL[i] - Q2 * sL[i]) * Fh[i]);
    }

    // printf("dadt: \n");
    // printStackVec(dadt, nquads);

    // fft inputs
    kiss_fft_scalar zero;
    memset(&zero,0,sizeof(zero)); 

    for (i = 0; i < nquads; i++) {
        dcoeFt1[i].r = dadt[i];
        dcoeFt1[i].i = zero;
        dcoeFt2[i].r = dP1dt[i];
        dcoeFt2[i].i = zero;
        dcoeFt3[i].r = dP2dt[i];
        dcoeFt3[i].i = zero;
        dcoeFt4[i].r = dQ1dt[i];
        dcoeFt4[i].i = zero;
        dcoeFt5[i].r = dQ2dt[i];
        dcoeFt5[i].i = zero;
        dcoeFt6[i].r = dldt[i];
        dcoeFt6[i].i = zero;
    }

    // perform FFT
    kiss_fft_cfg  kiss_fft_state;
    kiss_fft_state = kiss_fft_alloc(nquads, 0, 0, 0);
    kiss_fft(kiss_fft_state,dcoeFt1,coutfFT1);
    kiss_fft(kiss_fft_state,dcoeFt2,coutfFT2);
    kiss_fft(kiss_fft_state,dcoeFt3,coutfFT3);
    kiss_fft(kiss_fft_state,dcoeFt4,coutfFT4);
    kiss_fft(kiss_fft_state,dcoeFt5,coutfFT5);
    kiss_fft(kiss_fft_state,dcoeFt6,coutfFT6);

    // printf("coutfFt2: \n");
    // printComplexStackVec(coutfFT2, nquads);

    // printf("coutfFt1: \n");
    // printComplexStackVec(coutfFT1, nquads);

    // circular shift FFT outputs
    int shift = floor(nquads / 2.0);

    // printf("shift %d places\n", shift);

    kissCircShift(coutfFT1, nquads, shift);
    kissCircShift(coutfFT2, nquads, shift);
    kissCircShift(coutfFT3, nquads, shift);
    kissCircShift(coutfFT4, nquads, shift);
    kissCircShift(coutfFT5, nquads, shift);
    kissCircShift(coutfFT6, nquads, shift);

    // divide by nquads
    double div = nquads;
    for (i = 0; i < nquads; i++) {
        coutfFT1[i].r = coutfFT1[i].r / div;
        coutfFT1[i].i = coutfFT1[i].i / div;
        coutfFT2[i].r = coutfFT2[i].r / div;
        coutfFT2[i].i = coutfFT2[i].i / div;
        coutfFT3[i].r = coutfFT3[i].r / div;
        coutfFT3[i].i = coutfFT3[i].i / div;
        coutfFT4[i].r = coutfFT4[i].r / div;
        coutfFT4[i].i = coutfFT4[i].i / div;
        coutfFT5[i].r = coutfFT5[i].r / div;
        coutfFT5[i].i = coutfFT5[i].i / div;
        coutfFT6[i].r = coutfFT6[i].r / div;
        coutfFT6[i].i = coutfFT6[i].i / div;
    }

    // printf("shifted coutfFt1: \n");
    // printComplexStackVec(coutfFT1, nquads);

    // calculate transformed orbital element vector
    complex coeU[nquads][6];
    complex coeUb[nquads][6];

    // copy kiss array to complex array and flip it
    complex temp1[nquads];
    complex temp2[nquads];
    complex temp3[nquads];
    complex temp4[nquads];
    complex temp5[nquads];
    complex temp6[nquads];

    // copy
    kiss2Complex(coutfFT1, temp1, nquads);
    kiss2Complex(coutfFT2, temp2, nquads);
    kiss2Complex(coutfFT3, temp3, nquads);
    kiss2Complex(coutfFT4, temp4, nquads);
    kiss2Complex(coutfFT5, temp5, nquads);
    kiss2Complex(coutfFT6, temp6, nquads);

    // flip (multiply by -1i)
    complexFlip(temp1, nquads);
    complexFlip(temp2, nquads);
    complexFlip(temp3, nquads);
    complexFlip(temp4, nquads);
    complexFlip(temp5, nquads);
    complexFlip(temp6, nquads);
       
    // element-wise division by wComb
    for (i = 0; i < nquads; i++) {
        coeU[i][0].r = temp1[i].r / wComb[i];
        coeU[i][0].i = temp1[i].i / wComb[i];
        coeU[i][1].r = temp2[i].r / wComb[i];
        coeU[i][1].i = temp2[i].i / wComb[i];
        coeU[i][2].r = temp3[i].r / wComb[i];
        coeU[i][2].i = temp3[i].i / wComb[i];
        coeU[i][3].r = temp4[i].r / wComb[i];
        coeU[i][3].i = temp4[i].i / wComb[i];
        coeU[i][4].r = temp5[i].r / wComb[i];
        coeU[i][4].i = temp5[i].i / wComb[i];
        coeU[i][5].r = temp6[i].r / wComb[i];
        coeU[i][5].i = temp6[i].i / wComb[i];
    }

    // compute coeUb
    for (i = 0; i < nquads; i++) {
        coeUb[i][0].r = -1.0 * coutfFT1[i].r / (wComb[i] * wComb[i]);
        coeUb[i][0].i = -1.0 * coutfFT1[i].i / (wComb[i] * wComb[i]);
        coeUb[i][1].r = -1.0 * coutfFT2[i].r / (wComb[i] * wComb[i]);
        coeUb[i][1].i = -1.0 * coutfFT2[i].i / (wComb[i] * wComb[i]);
        coeUb[i][2].r = -1.0 * coutfFT3[i].r / (wComb[i] * wComb[i]);
        coeUb[i][2].i = -1.0 * coutfFT3[i].i / (wComb[i] * wComb[i]);
        coeUb[i][3].r = -1.0 * coutfFT4[i].r / (wComb[i] * wComb[i]);
        coeUb[i][3].i = -1.0 * coutfFT4[i].i / (wComb[i] * wComb[i]);
        coeUb[i][4].r = -1.0 * coutfFT5[i].r / (wComb[i] * wComb[i]);
        coeUb[i][4].i = -1.0 * coutfFT5[i].i / (wComb[i] * wComb[i]);
        coeUb[i][5].r = -1.0 * coutfFT6[i].r / (wComb[i] * wComb[i]);
        coeUb[i][5].i = -1.0 * coutfFT6[i].i / (wComb[i] * wComb[i]);
    }

    // printf("i0 = %d\n",i0);
    // set elements at i0 eqaul to 0
    for (i = 0; i < 6; i++) {
        coeU[i0 - 1][i].r  = 0.0;
        coeU[i0 - 1][i].i  = 0.0;
        coeUb[i0 - 1][i].r = 0.0;
        coeUb[i0 - 1][i].i = 0.0;
    }

    // compute expPhi
    complex expPhi[nquads];
    double X[nquads];

    for (i = 0; i < nquads; i++) {
        X[i] = idxX[i] * l;
    }

    eulerEqArr(X, nquads, expPhi);
    
    // compute u array
    double u[6];
    double ub[6];

    u[0]  = 0;
    u[1]  = 0;
    u[2]  = 0;
    u[3]  = 0;
    u[4]  = 0;
    u[5]  = 0;
    ub[0] = 0;
    ub[1] = 0;
    ub[2] = 0;
    ub[3] = 0;
    ub[4] = 0;
    ub[5] = 0;

    for (i = 0; i < nquads; i ++) {
        u[0] = u[0] +  (coeU[i][0].r * expPhi[i].r - coeU[i][0].i * expPhi[i].i);
        u[1] = u[1] +  (coeU[i][1].r * expPhi[i].r - coeU[i][1].i * expPhi[i].i);
        u[2] = u[2] +  (coeU[i][2].r * expPhi[i].r - coeU[i][2].i * expPhi[i].i);
        u[3] = u[3] +  (coeU[i][3].r * expPhi[i].r - coeU[i][3].i * expPhi[i].i);
        u[4] = u[4] +  (coeU[i][4].r * expPhi[i].r - coeU[i][4].i * expPhi[i].i);
        u[5] = u[5] +  (coeU[i][5].r * expPhi[i].r - coeU[i][5].i * expPhi[i].i);

        ub[0] = ub[0] +  (coeUb[i][0].r * expPhi[i].r - coeUb[i][0].i * expPhi[i].i);
        ub[1] = ub[1] +  (coeUb[i][1].r * expPhi[i].r - coeUb[i][1].i * expPhi[i].i);
        ub[2] = ub[2] +  (coeUb[i][2].r * expPhi[i].r - coeUb[i][2].i * expPhi[i].i);
        ub[3] = ub[3] +  (coeUb[i][3].r * expPhi[i].r - coeUb[i][3].i * expPhi[i].i);
        ub[4] = ub[4] +  (coeUb[i][4].r * expPhi[i].r - coeUb[i][4].i * expPhi[i].i);
        ub[5] = ub[5] +  (coeUb[i][5].r * expPhi[i].r - coeUb[i][5].i * expPhi[i].i);
    }

    // printf("ub:\n");
    // printStackVec(ub, 6);
 
    double add = 0;

    for (i = 0; i < 6; i++) {
        add = add + (dndx[i] * ub[i]);
    }

    u[5] = u[5] + add;

    // printf("u:\n");
    // printStackVec(u, 6);

    // direction of conversion
    double dir;
    if (type == 'm') {
        dir = 1.0;
    }
    else {
        dir = -1.0;
    }

    // add short period correction
    double coepeq[6];

    coepeq[0] = coeeq[0] + dir * u[0];
    coepeq[1] = coeeq[1] + dir * u[1];
    coepeq[2] = coeeq[2] + dir * u[2];
    coepeq[3] = coeeq[3] + dir * u[3];
    coepeq[4] = coeeq[4] + dir * u[4];
    coepeq[5] = coeeq[5] + dir * u[5];

    // Re-converting to Keplerian elements
    double ap  = coepeq[0];
    double ep  = sqrt(coepeq[1] * coepeq[1] + coepeq[2] * coepeq[2]);
    double ip  = 2 * atan(sqrt(coepeq[3] * coepeq[3] + coepeq[4] * coepeq[4]));
    double Omp = atan2(coepeq[3], coepeq[4]);

    if (Omp < 0.0) {
        Omp = Omp + 2 * M_PI;
    }

    double wp = wrap2pi(atan2(coepeq[1], coepeq[2]) - Omp);

    if (wp < 0.0) {
        wp = wp + 2 * M_PI;
    }

    double Mp = wrap2pi(coepeq[5] - wp - Omp);

    if (Mp < 0.0) {
        Mp = Mp + 2 * M_PI;
    }

    // add to output array
    coep[0] = ap;
    coep[1] = ep;
    coep[2] = ip;
    coep[3] = Omp;
    coep[4] = wp;
    coep[5] = Mp;

    // printf("coep:\n");
    // printStackVec(coep, 6);
    

}