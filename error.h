// recursive helper function to compute mean error
void meanErrHelper(OBJS *conNode, OBJS *solNode, double err[6]) {

    int i;
    double stateErr[6];
    
    if (conNode == NULL || solNode == NULL) {
        return;
    }
    else {

        // compute error
        for (i = 0; i < 6; i++) {
            stateErr[i] = fabs(conNode->COEmean[i] - solNode->COEmean[i]);
            err[i] = err[i] + stateErr[i];
            // printf("%d -> %.15f --- %.15f\n", conNode->idx, conNode->COEosc[i], solNode->COEosc[i]);
        }

        // recurse
        meanErrHelper(conNode->next, solNode->next, err);

        return;

    }


}

// Function to compute error between mean elements of two lists
void meanErr(OBJS *converted, OBJS *solution) {

    int i;

    // number of states
    double n = converted->tail->idx;

    // error array
    double err[6];

    // zero error array
    for (i = 0; i < 6; i++) {
        err[i] = 0;
    }

    // call helper function
    meanErrHelper(converted->head, solution->head, err);

    // average of error
    printf("AVERAGE ERROR\n");
    for (i = 0; i < 6; i++) {
        err[i] = err[i] / n;
        printf("%d -> %.25f\n", i, err[i]);
    }
    

}

// recursive helper function to compute osculating error
void oscErrHelper(OBJS *conNode, OBJS *solNode, double err[6]) {

    int i;
    double stateErr[6];
    
    if (conNode == NULL || solNode == NULL) {
        return;
    }
    else {

        // compute error
        for (i = 0; i < 6; i++) {
            stateErr[i] = fabs(conNode->COEosc[i] - solNode->COEosc[i]);
            err[i] = err[i] + stateErr[i];
            // printf("%d -> %.15f --- %.15f\n", conNode->idx, conNode->COEosc[i], solNode->COEosc[i]);
        }

        // recurse
        oscErrHelper(conNode->next, solNode->next, err);

        return;

    }


}

// Function to compute error between mean elements of two lists
void oscErr(OBJS *converted, OBJS *solution) {

    int i;

    // number of states
    double n = converted->tail->idx;

    // error array
    double err[6];

    // zero error array
    for (i = 0; i < 6; i++) {
        err[i] = 0;
    }

    // call helper function
    oscErrHelper(converted->head, solution->head, err);

    // average of error
    printf("AVERAGE ERROR\n");
    for (i = 0; i < 6; i++) {
        err[i] = err[i] / n;
        printf("%d -> %.25f\n", i, err[i]);
    }

}

// recursive helper function to compute osculating between optimal and initial states
void optiErrHelper(OBJS *conNode, OBJS *solNode, double err[6]) {

    int i;
    double stateErr[5];
    double lcon;
    double lsol;
    
    if (conNode == NULL || solNode == NULL) {
        return;
    }
    else {

        // compute error
        // printf("%f -- %f\n", conNode->COEopti[3], solNode->COEosc[3]);
        stateErr[0] = fabs(conNode->COEopti[0] - solNode->COEosc[0]);
        stateErr[1] = fabs(conNode->COEopti[1] - solNode->COEosc[1]);
        stateErr[2] = fabs(conNode->COEopti[2] - solNode->COEosc[2]);
        stateErr[3] = fabs(conNode->COEopti[3] - solNode->COEosc[3]);
        lcon        = wrap360(conNode->COEopti[4] + conNode->COEopti[5]);
        lsol        = wrap360(solNode->COEosc[4] + solNode->COEosc[5]);
        stateErr[4] = fabs(lcon - lsol);

        // sum
        err[0] = err[0] + stateErr[0];
        err[1] = err[1] + stateErr[1];
        err[2] = err[2] + stateErr[2];
        err[3] = err[3] + stateErr[3];
        err[4] = err[4] + stateErr[4];

        // recurse
        optiErrHelper(conNode->next, solNode->next, err);

        return;

    }


}

// Function to compute error between mean elements of two lists
void optiErr(OBJS *converted, OBJS *solution) {

    int i;

    // number of states
    double n = converted->tail->idx;

    // error array
    double err[5];

    // zero error array
    for (i = 0; i < 5; i++) {
        err[i] = 0;
    }

    // call helper function
    optiErrHelper(converted->head, solution->head, err);

    // average of error
    printf("AVERAGE ERROR\n");
    for (i = 0; i < 5; i++) {
        err[i] = err[i] / (n + 1);
        printf("%d -> %.25f\n", i, err[i]);
    }

}
    

