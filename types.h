// units
typedef double second;
typedef int iyear;
typedef int imonth;
typedef int iday;
typedef int ihour;
typedef int iminute;
typedef int imjd;
typedef double mjd;
typedef double radian;
typedef double rads;
typedef double arcsecond;
typedef double miliarcsecond;
typedef double milisecond;
typedef double km;
typedef double kms;
typedef double microsecond;
typedef double jdUTC;
typedef double jdUT1;
typedef double jdTAI;
typedef double jdTT;
typedef int ijdTT;
typedef double jdGPS;
typedef double clockroc; // unit equal to 10**-4 microseconds/second
typedef double kg;
typedef double orbels;

// array types
typedef double rotmat;

// struct for reading in target/field objects
struct obj {
    int idx;
    mjd mjd0;
    orbels COEosc[6];
    orbels COEmean[6];
    orbels COEoscf[6];
    orbels COEmeanf[6];
    orbels COEopti[6];
    orbels COEopAvg[6];
    kg SCMass;
    double Adrag;
    double ASRP;
    double CD;
    double CR;
    struct obj *prev;
    struct obj *next;
    struct obj *head;
    struct obj *tail;
};

typedef struct obj OBJS;

// propagation input type
struct Input
{
    char outputDir[MAXLINE];
    char objects[MAXLINE];
    int numObjs;
    int insgrav;
    int isun;
    int imoon;
    int idrag;
    int iF107;
    int iSRP;
    int iephem;
    int gdeg;
    int gord;
    double tol;
    double tspan; 
    double tstep;
    int imcoll;
    int eqs;
};

typedef struct Input INPUT;

// propagation input objects
struct objectsIn
{
    double **states;
    double **timeSpans;
    mjd initialEpoch;
};

typedef struct objectsIn OBJECTSIN;

// error state for calculating rms
struct errorState {
    km x;
    km y;
    km z;
    km r;
    km s;
    km w;
    kms xdot;
    kms ydot;
    kms zdot;
    orbels COE[6];
};

typedef struct errorState ERR;

// complex number type
typedef struct {
    double r;
    double i;
} complex;


