// recursive helper function for oscList2Avg and avgList2Osc
void recurseList(OBJS *node, const char elType) {

    if (node != NULL) {
        
        // zonal coefficients
        double Ji[15];

        // normalized zonal coefficients from GRIM
        double C20  = -4.84165115570150018E-04;
        double C30  = 9.58574916471960067E-07;
        double C40  = 5.39787841725119966E-07;
        double C50  = 6.72043988903809951E-08;
        double C60  = -1.49852403801750000E-07;
        double C70  = 9.31136691833619994E-08;
        double C80  = 5.04645062929899971E-08;
        double C90  = 2.62076310144059992E-08;
        double C100 = 5.07619144933030026E-08;
        double C110 = -5.02353915950460007E-08;
        double C120 = 4.21820883377799989E-08;
        double C130 = 4.22773579137330000E-08;
        double C140 = -3.13506139224660021E-08;
        double C150 = -2.85893335185249987E-11;
    
        // non-normalized
        Ji[0] = 0.0;
        Ji[1] = -C20*sqrt(1 + 2 * 2);
        Ji[2] = -C30*sqrt(1 + 3 * 2);
        Ji[3] = -C40*sqrt(1 + 4 * 2);
        Ji[4]  = -C50*sqrt(1 + 5 * 2);
        Ji[5]  = -C60*sqrt(1 + 6 * 2);
        Ji[6]  = -C70*sqrt(1 + 7 * 2);
        Ji[7]  = -C80*sqrt(1 + 8 * 2);
        Ji[8]  = -C90*sqrt(1 + 9 * 2);
        Ji[9] = -C100*sqrt(1 + 10 * 2);
        Ji[10] = -C110*sqrt(1 + 11 * 2);
        Ji[11] = -C120*sqrt(1 + 12 * 2);
        Ji[12] = -C130*sqrt(1 + 13 * 2);
        Ji[13] = -C140*sqrt(1 + 14 * 2);
        Ji[14] = -C150*sqrt(1 + 15 * 2);

        // from stela
        Ji[0] = 0.0;
        Ji[1] = 0.10826261107488972E-02;
        Ji[2] = -0.25361508420093188E-05;
        Ji[3] = -0.16193635251753600E-05;
        Ji[4] = -0.22289177538077055E-06;
        Ji[5] = 0.54030052565874444E-06;
        Ji[6] = -0.36062769005142777E-06;
        Ji[7] = -0.20807048979064489E-06;
        Ji[8] = -0.11423641514139811E-06;
        Ji[9] = -0.23262031558643691E-06;
        Ji[10] = 0.24092047459748041E-06;
        Ji[11] = -0.21091044168889998E-06;
        Ji[12] = -0.21967959574907912E-06;
        Ji[13] = 0.16882822277732491E-06;
        Ji[14] = 0.15917867232150533E-09;          


        // Ji[0] = 0.0;
        // Ji[1] = 1.082635854e-3;
        // Ji[2] = -2.532435346e-6;
        // Ji[3] =  -1.619331205e-6;
        // Ji[4] = -2.277161016e-6;
        // Ji[5] = 5.396484906e-7;
        // Ji[6] = - 3.513684422e-7;
        // Ji[7] = - 2.025187152e-7;

        Ji[0] = 0.0;
        Ji[3] =  0;
        Ji[5] = 0;
        Ji[7] = 0;
        Ji[9] = 0;
        Ji[11] = 0;
        Ji[13] = 0;

        if (elType == 'o') {

            // initial conditions in km and rads
            double coe[6];

            coe[0] = node->COEosc[0];
            // printf("%.25f\n",node->COEosc[0]);
            coe[1] = node->COEosc[1];
            coe[2] = node->COEosc[2]*d2r;
            coe[3] = node->COEosc[3]*d2r;
            coe[4] = node->COEosc[4]*d2r;
            coe[5] = node->COEosc[5]*d2r;

            // number of points on fft
            int nquads = 401;

            // run numerical routine
            mean2osc_zonal(coe, nquads, Ji, 5, elType, node->COEmean);

            // convert to degrees
            node->COEmean[2] = node->COEmean[2]*r2d;
            node->COEmean[3] = node->COEmean[3]*r2d;
            node->COEmean[4] = node->COEmean[4]*r2d;
            node->COEmean[5] = node->COEmean[5]*r2d;

            // recurse
            recurseList(node->next, elType);
        }
        else if (elType == 'm') {

            // initial conditions in km and rads
            double coe[6];

            coe[0] = node->COEmean[0];
            coe[1] = node->COEmean[1];
            coe[2] = node->COEmean[2]*d2r;
            coe[3] = node->COEmean[3]*d2r;
            coe[4] = node->COEmean[4]*d2r;
            coe[5] = node->COEmean[5]*d2r;

            // number of points on fft
            int nquads = 401;

            // run numerical routine
            mean2osc_zonal(coe, nquads, Ji, 5, elType, node->COEosc);

            // convert to degrees
            node->COEosc[2] = node->COEosc[2]*r2d;
            node->COEosc[3] = node->COEosc[3]*r2d;
            node->COEosc[4] = node->COEosc[4]*r2d;
            node->COEosc[5] = node->COEosc[5]*r2d;

            // recurse
            recurseList(node->next, elType);

        }
        else {
            printf("PLEASE ENTER VALID ELEMENT TYPE ('o' or 'm')\n");
            exit(EXIT_FAILURE);
        }
    }

} 

// function to convert a linked obj list
// from osculating to mean elements using
// lamberto's numerical routine
void oscList2Avg(OBJS *states) {

    // call helper functions
    recurseList(states->head, 'o');

}

// function to convert a linked obj list
// from mean to osculating elements using
// lamberto's numerical routine
void avgList2Osc(OBJS *states) {

    // call helper functions
    recurseList(states->head, 'm');

}

// recursive helper function for avgList2OscFrozen
void recurseFrozenAvgList(OBJS *node) {

    if (node != NULL) {

        // zonal coefficients
        double Ji[15];

        // normalized zonal coefficients from GRIM
        double C20  = -4.84165115570150018E-04;
        double C30  = 9.58574916471960067E-07;
        double C40  = 5.39787841725119966E-07;
        double C50  = 6.72043988903809951E-08;
        double C60  = -1.49852403801750000E-07;
        double C70  = 9.31136691833619994E-08;
        double C80  = 5.04645062929899971E-08;
        double C90  = 2.62076310144059992E-08;
        double C100 = 5.07619144933030026E-08;
        double C110 = -5.02353915950460007E-08;
        double C120 = 4.21820883377799989E-08;
        double C130 = 4.22773579137330000E-08;
        double C140 = -3.13506139224660021E-08;
        double C150 = -2.85893335185249987E-11;
    
        // non-normalized
        Ji[0] = 0.0;
        Ji[1] = -C20*sqrt(1 + 2 * 2);
        Ji[2] = -C30*sqrt(1 + 3 * 2);
        Ji[3] = -C40*sqrt(1 + 4 * 2);
        Ji[4]  = -C50*sqrt(1 + 5 * 2);
        Ji[5]  = -C60*sqrt(1 + 6 * 2);
        Ji[6]  = -C70*sqrt(1 + 7 * 2);
        Ji[7]  = -C80*sqrt(1 + 8 * 2);
        Ji[8]  = -C90*sqrt(1 + 9 * 2);
        Ji[9] = -C100*sqrt(1 + 10 * 2);
        Ji[10] = -C110*sqrt(1 + 11 * 2);
        Ji[11] = -C120*sqrt(1 + 12 * 2);
        Ji[12] = -C130*sqrt(1 + 13 * 2);
        Ji[13] = -C140*sqrt(1 + 14 * 2);
        Ji[14] = -C150*sqrt(1 + 15 * 2);

         // from stela
        Ji[0] = 0.0;
        Ji[1] = 0.10826261107488972E-02;
        Ji[2] = -0.25361508420093188E-05;
        Ji[3] = -0.16193635251753600E-05;
        Ji[4] = -0.22289177538077055E-06;
        Ji[5] = 0.54030052565874444E-06;
        Ji[6] = -0.36062769005142777E-06;
        Ji[7] = -0.20807048979064489E-06;
        Ji[8] = -0.11423641514139811E-06;
        Ji[9] = -0.23262031558643691E-06;
        Ji[10] = 0.24092047459748041E-06;
        Ji[11] = -0.21091044168889998E-06;
        Ji[12] = -0.21967959574907912E-06;
        Ji[13] = 0.16882822277732491E-06;
        Ji[14] = 0.15917867232150533E-09;       

        // Ji[0] = 0.0;
        // Ji[1] = 1.082635854e-3;
        // Ji[2] = -2.532435346e-6;
        // Ji[3] =  -1.619331205e-6;
        // Ji[4] = -2.277161016e-6;
        // Ji[5] = 5.396484906e-7;
        // Ji[6] = - 3.513684422e-7;
        // Ji[7] = - 2.025187152e-7;

        // Ji[0] = 0.0;
        // Ji[1] = 1.082635854e-3;
        // Ji[2] = -2.532435346e-6;
        // Ji[3] =  0;
        // Ji[4] = -2.277161016e-6;
        // Ji[5] = 0;
        // Ji[6] = - 3.513684422e-7;
        // Ji[7] = -0;

        Ji[0] = 0.0;
        Ji[3] =  0;
        Ji[5] = 0;
        Ji[7] = 0;
        Ji[9] = 0;
        Ji[11] = 0;
        Ji[13] = 0;


        // initial conditions in km and rads
        double coe[6];

        coe[0] = node->COEmeanf[0];
        coe[1] = node->COEmeanf[1];
        coe[2] = node->COEmeanf[2]*d2r;
        coe[3] = node->COEmeanf[3]*d2r;
        coe[4] = node->COEmeanf[4]*d2r;
        coe[5] = node->COEmeanf[5]*d2r;

        // number of points on fft
        int nquads = 401;

        // run numerical routine
        mean2osc_zonal(coe, nquads, Ji, 5, 'm', node->COEoscf);

        // convert to degrees
        node->COEoscf[2] = node->COEoscf[2]*r2d;
        node->COEoscf[3] = node->COEoscf[3]*r2d;
        node->COEoscf[4] = node->COEoscf[4]*r2d;
        node->COEoscf[5] = node->COEoscf[5]*r2d;

        // recurse
        recurseFrozenAvgList(node->next);

    }
        
} 

// function to convert a linked obj list
// from frozen mean to frozen osculating 
// elements using lamberto's numerical 
// routine.
void avgList2OscFrozen(OBJS *states) {

    // call helper functions
    recurseFrozenAvgList(states->head);

}

// recursive helper function for optiOscList2Avg 
void recurseOptiList(OBJS *node, const char elType) {

    if (node != NULL) {
        
        // zonal coefficients
        double Ji[15];

        // normalized zonal coefficients from GRIM
        double C20  = -4.84165115570150018E-04;
        double C30  = 9.58574916471960067E-07;
        double C40  = 5.39787841725119966E-07;
        double C50  = 6.72043988903809951E-08;
        double C60  = -1.49852403801750000E-07;
        double C70  = 9.31136691833619994E-08;
        double C80  = 5.04645062929899971E-08;
        double C90  = 2.62076310144059992E-08;
        double C100 = 5.07619144933030026E-08;
        double C110 = -5.02353915950460007E-08;
        double C120 = 4.21820883377799989E-08;
        double C130 = 4.22773579137330000E-08;
        double C140 = -3.13506139224660021E-08;
        double C150 = -2.85893335185249987E-11;
    
        // non-normalized
        Ji[0] = 0.0;
        Ji[1] = -C20*sqrt(1 + 2 * 2);
        Ji[2] = -C30*sqrt(1 + 3 * 2);
        Ji[3] = -C40*sqrt(1 + 4 * 2);
        Ji[4]  = -C50*sqrt(1 + 5 * 2);
        Ji[5]  = -C60*sqrt(1 + 6 * 2);
        Ji[6]  = -C70*sqrt(1 + 7 * 2);
        Ji[7]  = -C80*sqrt(1 + 8 * 2);
        Ji[8]  = -C90*sqrt(1 + 9 * 2);
        Ji[9] = -C100*sqrt(1 + 10 * 2);
        Ji[10] = -C110*sqrt(1 + 11 * 2);
        Ji[11] = -C120*sqrt(1 + 12 * 2);
        Ji[12] = -C130*sqrt(1 + 13 * 2);
        Ji[13] = -C140*sqrt(1 + 14 * 2);
        Ji[14] = -C150*sqrt(1 + 15 * 2);

        // from stela
        Ji[0] = 0.0;
        Ji[1] = 0.10826261107488972E-02;
        Ji[2] = -0.25361508420093188E-05;
        Ji[3] = -0.16193635251753600E-05;
        Ji[4] = -0.22289177538077055E-06;
        Ji[5] = 0.54030052565874444E-06;
        Ji[6] = -0.36062769005142777E-06;
        Ji[7] = -0.20807048979064489E-06;
        Ji[8] = -0.11423641514139811E-06;
        Ji[9] = -0.23262031558643691E-06;
        Ji[10] = 0.24092047459748041E-06;
        Ji[11] = -0.21091044168889998E-06;
        Ji[12] = -0.21967959574907912E-06;
        Ji[13] = 0.16882822277732491E-06;
        Ji[14] = 0.15917867232150533E-09;       

        // Ji[0] = 0.0;
        // Ji[1] = 1.082635854e-3;
        // Ji[2] = -2.532435346e-6;
        // Ji[3] =  -1.619331205e-6;
        // Ji[4] = -2.277161016e-6;
        // Ji[5] = 5.396484906e-7;
        // Ji[6] = - 3.513684422e-7;
        // Ji[7] = - 2.025187152e-7;

        // Ji[0] = 0.0;
        // Ji[1] = 1.082635854e-3;
        // Ji[2] = -2.532435346e-6;
        // Ji[3] =  0;
        // Ji[4] = -2.277161016e-6;
        // Ji[5] = 0;
        // Ji[6] = - 3.513684422e-7;
        // Ji[7] = -0;

        Ji[0] = 0.0;
        Ji[3] =  0;
        Ji[5] = 0;
        Ji[7] = 0;
        Ji[9] = 0;
        Ji[11] = 0;
        Ji[13] = 0;

        if (elType == 'o') {

            // initial conditions in km and rads
            double coe[6];

            coe[0] = node->COEopti[0];
            coe[1] = node->COEopti[1];
            coe[2] = node->COEopti[2]*d2r;
            coe[3] = node->COEopti[3]*d2r;
            coe[4] = node->COEopti[4]*d2r;
            coe[5] = node->COEopti[5]*d2r;

            // number of points on fft
            int nquads = 401;

            // run numerical routine
            mean2osc_zonal(coe, nquads, Ji, 5, elType, node->COEopAvg);

            // convert to degrees
            node->COEopAvg[2] = node->COEopAvg[2]*r2d;
            node->COEopAvg[3] = node->COEopAvg[3]*r2d;
            node->COEopAvg[4] = node->COEopAvg[4]*r2d;
            node->COEopAvg[5] = node->COEopAvg[5]*r2d;

            // recurse
            recurseOptiList(node->next, elType);
        }
        else if (elType == 'm') {

            // initial conditions in km and rads
            double coe[6];

            coe[0] = node->COEopAvg[0];
            coe[1] = node->COEopAvg[1];
            coe[2] = node->COEopAvg[2]*d2r;
            coe[3] = node->COEopAvg[3]*d2r;
            coe[4] = node->COEopAvg[4]*d2r;
            coe[5] = node->COEopAvg[5]*d2r;

            // number of points on fft
            int nquads = 401;

            // run numerical routine
            mean2osc_zonal(coe, nquads, Ji, 5, elType, node->COEopti);

            // convert to degrees
            node->COEopti[2] = node->COEopti[2]*r2d;
            node->COEopti[3] = node->COEopti[3]*r2d;
            node->COEopti[4] = node->COEopti[4]*r2d;
            node->COEopti[5] = node->COEopti[5]*r2d;

            // recurse
            recurseOptiList(node->next, elType);

        }
        else {
            printf("PLEASE ENTER VALID ELEMENT TYPE ('o' or 'm')\n");
            exit(EXIT_FAILURE);
        }
    }

} 

// function to convert a linked obj list
// from optimized osculating to optimized
// mean elements using
// lamberto's numerical routine
void optiOscList2Avg(OBJS *states) {

    // call helper functions
    recurseOptiList(states->head, 'o');

}