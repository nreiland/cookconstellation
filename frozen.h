#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// (frozenCook avgSma avgInc degree) < This function computes the corresponding
// mean eccentricity based on the works of Brouwer and Cook. The function
// takes as arguments the mean semi-major axis in km, avgSma, the average
// inclination in degrees (degres 2 through 15 are supported), avgInc, as well 
// as the degree of the zonal coefficients. The function returns the mean 
// eccentricity in degrees of the corresponding frozen orbit.
double frozenCook(double avgSma, double avgInc, int degree) {

    // check for valid harmonics
    if (degree < 2 || degree > 15) {
        printf("PLEASE ENTER VALID DEGREE (between 2 and 15)\n");
        exit(EXIT_FAILURE);
    }

    // iterators
    int i;
    
    // normalized zonal coefficients from GRIM
    double C20  = -4.84165115570150018E-04;
    double C30  = 9.58574916471960067E-07;
    double C40  = 5.39787841725119966E-07;
    double C50  = 6.72043988903809951E-08;
    double C60  = -1.49852403801750000E-07;
    double C70  = 9.31136691833619994E-08;
    double C80  = 5.04645062929899971E-08;
    double C90  = 2.62076310144059992E-08;
    double C100 = 5.07619144933030026E-08;
    double C110 = -5.02353915950460007E-08;
    double C120 = 4.21820883377799989E-08;
    double C130 = 4.22773579137330000E-08;
    double C140 = -3.13506139224660021E-08;
    double C150 = -2.85893335185249987E-11;
 
    // non-normalized
    double J2  = -C20*sqrt(1 + 2 * 2);
    double J3  = -C30*sqrt(1 + 3 * 2);
    double J4  = -C40*sqrt(1 + 4 * 2);
    double J5  = -C50*sqrt(1 + 5 * 2);
    double J6  = -C60*sqrt(1 + 6 * 2);
    double J7  = -C70*sqrt(1 + 7 * 2);
    double J8  = -C80*sqrt(1 + 8 * 2);
    double J9  = -C90*sqrt(1 + 9 * 2);
    double J10 = -C100*sqrt(1 + 10 * 2);
    double J11 = -C110*sqrt(1 + 11 * 2);
    double J12 = -C120*sqrt(1 + 12 * 2);
    double J13 = -C130*sqrt(1 + 13 * 2);
    double J14 = -C140*sqrt(1 + 14 * 2);
    double J15 = -C150*sqrt(1 + 15 * 2);

    // place into array
    double J[16];

    J[0]  = 0;
    J[1]  = J2;
    J[2]  = J3;
    J[3]  = J4;
    J[4]  = J5;
    J[5]  = J6;
    J[6]  = J7;
    J[7]  = J8;
    J[8]  = J9;
    J[9]  = J10;
    J[10] = J11;
    J[11] = J12;
    J[12] = J13;
    J[13] = J14;
    J[14] = J15;
    J[15] = 0;

     // from stela
    J[0] = 0.0;
    J[1] = 0.10826261107488972E-02;
    J[2] = -0.25361508420093188E-05;
    J[3] = -0.16193635251753600E-05;
    J[4] = -0.22289177538077055E-06;
    J[5] = 0.54030052565874444E-06;
    J[6] = -0.36062769005142777E-06;
    J[7] = -0.20807048979064489E-06;
    J[8] = -0.11423641514139811E-06;
    J[9] = -0.23262031558643691E-06;
    J[10] = 0.24092047459748041E-06;
    J[11] = -0.21091044168889998E-06;
    J[12] = -0.21967959574907912E-06;
    J[13] = 0.16882822277732491E-06;
    J[14] = 0.15917867232150533E-09;       

    // select harmonics
    for (i = degree; i < 16; i++) {
        J[i] = 0;
        // printf("freezing %d\n",i + 1);
    }

    // compute Cook's approximation 
    double a;
    double kappa;
    double e_chao;
    double e_cook;

    a      = avgSma;
    kappa  = sin(avgInc*d2r) * sin(avgInc*d2r);
    e_chao = (J3 / (2 * J2)) * (REarth / a * sin(avgInc*d2r));
    e_cook = -0.5e1 / 0.2e1 * sqrt(kappa) * (0.29113619535e11 / 0.4194304e7 * \
                J[14] * (-0.16384e5 / 0.9694845e7 + 0.28672e5 / 0.570285e6 * kappa - \
                0.14336e5 / 0.30015e5 * pow(kappa, 2) + 0.12544e5 / 0.6003e4 * pow(kappa, 3) - \
                0.6272e4 / 0.1305e4 * pow(kappa, 4) + 0.1568e4 / 0.261e3 * pow(kappa, 5) - \
                0.112e3 / 0.29e2 * pow(kappa, 6) + pow(kappa, 7)) * pow(REarth, 12) + 0.111546435e9 / \
                0.65536e5 * J[12] * pow(a, 2) * (0.1024e4 / 0.185725e6 + 0.384e3 / 0.437e3 * \
                pow(kappa, 2) - 0.4608e4 / 0.37145e5 * kappa - 0.84e2 / 0.25e2 * pow(kappa, 5) + \
                pow(kappa, 6) - 0.64e2 / 0.23e2 * pow(kappa, 3) + 0.504e3 / 0.115e3 * pow(kappa, 4)) * \
                pow(REarth, 10) + 0.6789783e7 / 0.16384e5 * J[10] * (-0.512e3 / 0.29393e5 + \
                0.640e3 / 0.2261e4 * kappa - 0.3200e4 / 0.2261e4 * pow(kappa, 2) + 0.400e3 \
                / 0.133e3 * pow(kappa, 3) - 0.20e2 / 0.7e1 * pow(kappa, 4) + pow(kappa, 5)) * pow(a, 4) * \
                pow(REarth, 8) + 0.51051e5 / 0.512e3 * J[8] * (0.128e3 / 0.2431e4 - 0.128e3 / \
                0.221e3 * kappa + 0.32e2 / 0.17e2 * pow(kappa, 2) - 0.40e2 / 0.17e2 * \
                pow(kappa, 3) + pow(kappa, 4)) * pow(a, 6) * pow(REarth, 6) + 0.3003e4 / 0.128e3 * \
                (-0.64e2 / 0.429e3 + 0.144e3 / 0.143e3 * kappa - 0.24e2 / 0.13e2 * \
                pow(kappa, 2) + pow(kappa, 3)) * J[6] * pow(a, 8) * pow(REarth, 4) + 0.21e2 / 0.4e1 * J[4] * \
                (0.8e1 / 0.21e2 - 0.4e1 / 0.3e1 * kappa + pow(kappa, 2)) * pow(a, 10) * pow(REarth, 2) + \
                (-0.4e1 / 0.5e1 + kappa) * J[2] * pow(a, 12)) * REarth / pow(a, 13) / J2 / \
                (-0.4e1 + 0.5e1 * kappa);

    // return
    return e_cook;

}

// recursive helper functions
void recurseList2frozen(OBJS *node, int degree) {

    if (node != NULL) {
        
        // get frozen argument of perigee in degrees (always 90)
        double wF = 90;

        // get frozen eccentricity
        double avgSma = node->COEmean[0];
        double avgInc = node->COEmean[2];

        double eF = frozenCook(avgSma, avgInc, degree);

        // add to frozen array
        node->COEmeanf[0] = node->COEmean[0];
        node->COEmeanf[1] = eF;
        node->COEmeanf[2] = node->COEmean[2];
        node->COEmeanf[3] = node->COEmean[3];
        node->COEmeanf[4] = wF;
        node->COEmeanf[5] = node->COEmean[5];

        // recurse
        recurseList2frozen(node->next, degree);
    }
} 

// function to transform a linked obj list
// of mean elements to the corresponding
// frozen orbit according to the theories
// of Brouwer and Cook.
void avgList2frozen(OBJS *states, int degree) {

    // call helper functions
    recurseList2frozen(states->head, degree);

}
