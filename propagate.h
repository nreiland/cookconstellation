#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// helper function to pick time step
// with minimal residual wrt to the 
// object initial osculating condition.
void grabBest(double COEdes[6], double COEopti[6], double *traj, int trajLength) {

    // iterators
    int i;
    int minSmaIdx;
    int minMaIdx;
    int totMinIdx;

    // current orbital Elements
    double COEi[6];

    // error
    double currErr[6];

    // first iteration
    COEi[0] = traj[0 * 7 + 1];
    COEi[1] = traj[0 * 7 + 2];
    COEi[2] = traj[0 * 7 + 3];
    COEi[3] = traj[0 * 7 + 4];
    COEi[4] = traj[0 * 7 + 5];
    COEi[5] = traj[0 * 7 + 6];

    // current error
    double minSmaErr = fabs(COEdes[0] - COEi[0]);
    double minMaErr  = fabs(wrap360(COEdes[4] + COEdes[5]) - wrap360(COEi[4] + COEi[5]));
    double minTotErr = sqrt(pow(minSmaErr, 2) + pow(minMaErr, 2));

    // loop through remaining trajectory
    for (i = 1; i < trajLength; i++) {

        // current oe
        COEi[0] = traj[i * 7 + 1];
        COEi[1] = traj[i * 7 + 2];
        COEi[2] = traj[i * 7 + 3];
        COEi[3] = traj[i * 7 + 4];
        COEi[4] = traj[i * 7 + 5];
        COEi[5] = traj[i * 7 + 6];

        // current error
        currErr[0] = fabs(COEdes[0] - COEi[0]);
        currErr[1] = fabs(COEdes[1] - COEi[1]);
        currErr[2] = fabs(COEdes[2] - COEi[2]);
        currErr[3] = fabs(COEdes[3] - COEi[3]);
        currErr[4] = fabs(COEdes[4] - COEi[4]);
        currErr[5] = fabs(wrap360(COEdes[4] + COEdes[5]) - wrap360(COEi[4] + COEi[5]));

        // get new sma err minimum
        if (currErr[0] < minSmaErr) {
            minSmaErr = currErr[0];
            minSmaIdx = i;
        }

        // get new ma err minimum
        if (currErr[5] < minMaErr) {
            minMaErr = currErr[5];
            minMaIdx = i;
        }

        // get new combined minimum
        if (sqrt(pow(currErr[0], 2) + pow(currErr[5], 2)) < minTotErr) {
            minTotErr = currErr[5];
            totMinIdx = i;
        }

    }

    // put in output array
    COEopti[0] = traj[minMaIdx * 7 + 1];
    COEopti[1] = traj[minMaIdx * 7 + 2];
    COEopti[2] = traj[minMaIdx * 7 + 3];
    COEopti[3] = traj[minMaIdx * 7 + 4];
    COEopti[4] = traj[minMaIdx * 7 + 5];
    COEopti[5] = traj[minMaIdx * 7 + 6];

}

// Helper function to get best IC of one
// orbital period.
void recurseAndMinimizeList(OBJS *node) {

    if (node != NULL) {

        // start clock
        double clockStart = clock();

        // thalassa input struct definition
        thalassaInput in;

        // propagation start date
        in.MJD0 = node->mjd0;

        // initial conditions 
        in.COE0[0] = node->COEoscf[0];
        in.COE0[1] = node->COEoscf[1];
        in.COE0[2] = node->COEoscf[2];
        in.COE0[3] = node->COEoscf[3];
        in.COE0[4] = node->COEoscf[4];
        in.COE0[5] = node->COEoscf[5];

        // spacecraft physical parameters
        in.SCMass = node->SCMass;
        in.ADrag  = node->Adrag;
        in.ASRP   = node->ASRP;
        in.CD     = node->CD;
        in.CR     = node->CR;

        // propagation time span and step
        in.tspan = (2 * M_PI * sqrt(pow(in.COE0[0], 3) / MU) / 86400);
        in.tstep = 1.0 / 86400.0;

        // pure zonal physical model
        in.insgrav = 1;
        in.isun    = 0;
        in.imoon   = 0;
        in.idrag   = 0;
        in.iF107   = 0;
        in.iSRP    = 0;
        in.iephem  = 1;
        in.gdeg    = 15;
        in.gord    = 0;

        // integration tolerance
        in.tol = 1.0e-12;

        // no moon collision
        in.imcoll = 0;

        // equations of montion - EDromo(t)
        in.eqs = 2;

        // propagate
        thalassa(&in);

        // minimize error
        grabBest(node->COEosc, node->COEopti, in.orbs, in.npts);

        // stop clock
        double clockEnd = clock();
        double timeUsed = (clockEnd - clockStart)/CLOCKS_PER_SEC;

        // output message
        printf("%f seconds on job %d with %d pts\n",timeUsed, node->idx, in.npts);

        // // free trajectories
        // free(in.orbs);
        // free(in.cart);

       

        // recurse
        recurseAndMinimizeList(node->next);

    }
        
} 

// Function to propagate for one orbital period and find
// the initial condition that minimizes the residuals
// between the frozen and intial orbits.
void getBestIC(OBJS *states) {

    // call helper functions
    recurseAndMinimizeList(states->head);

}