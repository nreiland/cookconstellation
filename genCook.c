#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "./SOFA/sofa.h"
#include "constants.h"
#include "types.h"
#include "arrays.h"
#include "readobjs.h"
#include "propInfo.h"
#include "mean2osc.h"
#include "mean2osclam.h"
#include "convertList.h"
#include "frozen.h"
#include "output.h"
#include "./thalassa/thalassa.h"
#include "propagate.h"
#include "error.h"
#include "tests.h"
// #include "error.h"

/*main function*/
int main( int argc, char *argv[] )
{
    // mpi variables
    int  numtasks, rank;

    // target file
    char targetFile[100] = "./in/onewebtargetControl.txt";

    // field file
    char fieldFile[100] = "./in/onewebfieldControl.txt";

    // frozen output files
    char frozenTargetAvg[100] = "./output/frozenTargetAvg.txt";
    char frozenFieldAvg[100]  = "./output/frozenFieldAvg.txt";
    char frozenTargetOsc[100] = "./output/frozenTargetOsc.txt";
    char frozenFieldOsc[100]  = "./output/frozenFieldOsc.txt";

    // nominal output files
    char nominalTargetAvg[100] = "./output/nominalTargetAvg.txt";
    char nominalFieldAvg[100]  = "./output/nominalFieldAvg.txt";
    char nominalTargetOsc[100] = "./output/nominalTargetOsc.txt";
    char nominalFieldOsc[100]  = "./output/nominalFieldOsc.txt";

    // optimal frozen outputs
    char optiFrozenTargetOsc[100] = "./output/optimalFrozenTargetOsc.txt";
    char optiFrozenFieldOsc[100] = "./output/optimalFrozenFieldOsc.txt";

    // mean optimal frozen outputs
    char optiFrozenTargetAvg[100] = "./output/optimalFrozenTargetAvg.txt";
    char optiFrozenFieldAvg[100] = "./output/optimalFrozenFieldAvg.txt";


    // residual files
    char frozenTargetErr[100] = "./output/frozenTargetErr.txt";
    char frozenFieldErr[100]  = "./output/frozenFieldErr.txt";

    // parse target object data
    OBJS *targets = readobjs(targetFile, 'p', 'o');

    // parse field object data
    OBJS *fields = readobjs(fieldFile, 'p', 'o');

    // load propagation settings
    INPUT *settings = loadPropInputs();

    // convert target objects to mean elements
    oscList2Avg(targets);

    // convert field objects to mean elements
    oscList2Avg(fields);

    // convert target objects to frozen mean elements
    avgList2frozen(targets, 5);

    // convert field objects to frozen mean elements
    avgList2frozen(fields, 5);

    // convert target frozen objects to osculating elements
    avgList2OscFrozen(targets);

    // convert field frozen objects to osculating elements
    avgList2OscFrozen(fields);

    // find `optimal` frozen osculating for target objects
    getBestIC(targets);

    // find `optimal` frozen osculating for field objects
    getBestIC(fields);

    // convert `optimal` targets from osculating to mean
    optiOscList2Avg(targets);

    // convert `optimal` fields from osculating to mean
    optiOscList2Avg(fields);

    // compute residuals
    optiErr(targets, targets);
    optiErr(fields, fields);

    // print frozen output
    writeElementList(targets, frozenTargetAvg, 'z', 'n');
    writeElementList(fields, frozenFieldAvg, 'z', 'n');
    writeElementList(targets, frozenTargetOsc, 'f', 'n');
    writeElementList(fields, frozenFieldOsc, 'f', 'n');

    // print nominal output
    writeElementList(targets, nominalTargetAvg, 'm', 'n');
    writeElementList(fields, nominalFieldAvg, 'm', 'n');
    writeElementList(targets, nominalTargetOsc, 'o', 'n');
    writeElementList(fields, nominalFieldOsc, 'o', 'n');

    // print optimal osculating output
    writeElementList(targets, optiFrozenTargetOsc, 'l', 'p');
    writeElementList(fields, optiFrozenFieldOsc, 'l', 'p');

    // print optimal mean output
    writeElementList(targets, optiFrozenTargetAvg, 'q', 'p');
    writeElementList(fields, optiFrozenFieldAvg, 'q', 'p');

}