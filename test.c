#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "./SOFA/sofa.h"
#include "constants.h"
#include "types.h"
#include "arrays.h"
#include "readobjs.h"
#include "propInfo.h"
#include "error.h"
#include "mean2osc.h"
#include "mean2osclam.h"
#include "convertList.h"
#include "frozen.h"
#include "tests.h"
// #include "error.h"

/*main function*/
int main( int argc, char *argv[] )
{

    // test computing mean elements
    //test1();

    // test analytical technique:
    // test2();

    // test numerical technique:
    // test3();

    // test numerical technique by reading in a trajectory 
    // and converting it to mean elements
    test4();
}