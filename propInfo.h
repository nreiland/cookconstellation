INPUT *loadPropInputs() {
    INPUT *input = malloc(sizeof(INPUT));
    
    strcpy(input->outputDir, "./output");
    
    input->insgrav = 1;
    input->isun    = 1;
    input->imoon   = 1;
    input->idrag   = 4;
    input->iF107   = 1;
    input->iSRP    = 2;
    input->iephem  = 1;
    input->gdeg    = 40;
    input->gord    = 40;
    input->tol     = 1.00000E-14;
    input->imcoll  = 0;
    input->eqs     = 2;
    input->tstep   = 1;

    return input;
}