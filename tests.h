// testing analytical routine
void test1() {

    double coe[6];
    double coep[6];

    // oneweb example satellite
    coe[0] = 7578;
    coe[1] = 0.001;
    coe[2] = 87.9;
    coe[3] = 0;
    coe[4] = 0;
    coe[5] = 0;

    // test conversion from osculating to mean
    mean2oscIRDG(coe, 'd', coep, 'o');
    printStackVec(coep, 6);

}

// testing analytical routine
void test2() {

    double coe[6];
    double coep[6];

    // oneweb example satellite
    coe[0] = 7578;
    coe[1] = 0.1;
    coe[2] = 87.9;
    coe[3] = 20;
    coe[4] = 50;
    coe[5] = 210;

    // test conversion from osculating to mean
    mean2oscIRDG(coe, 'd', coep, 'o');
    printStackVec(coep, 6);

}

// test numerical routin

void test3() {

    double coe[6];
    double coep[6];
    double Ji[8];

    // zonal coefficients
    Ji[0] = 0.0;
    Ji[1] = 1.082635854e-3;
    Ji[2] = -2.532435346e-6;
    Ji[3] =  -1.619331205e-6;
    Ji[4] = -2.277161016e-6;
    Ji[5] = 5.396484906e-7;
    Ji[6] = - 3.513684422e-7;
    Ji[7] = - 2.025187152e-7;

    // osculating initial conditions
    coe[0] = 7000;
    coe[1] = 0.1;
    coe[2] = 64*d2r;
    coe[3] = 20*d2r;
    coe[4] = 30*d2r;
    coe[5] = 40*d2r;

    // number of points on fft
    int nquads = 401;

    // run numerical routine
    mean2osc_zonal(coe, nquads, Ji, 8, 'o', coep);
}

// test converting a full trajectory
void test4() {

    // field files
    char oscTrajFile[100] = "/Users/nreiland/Documents/mean2oscScripts/lam/oeOsc.txt";
    char avgTrajFile[100] = "/Users/nreiland/Documents/mean2oscScripts/lam/oeAvg.txt";
    char AvgFromOscFile[100] = "/Users/nreiland/Documents/mean2oscScripts/lam/avgFromOsc.txt";
    char OscFromAvgFile[100]  = "/Users/nreiland/Documents/mean2oscScripts/lam/oscFromAvg.txt";

    // parse osculating trajectory
    OBJS *oscTraj = readobjs(oscTrajFile, 'n', 'o');

    // parse averaged trajectory
    OBJS *avgTraj = readobjs(avgTrajFile, 'n', 'm');

    // parse osculating from mean trajectory
    OBJS *oscFromAvgTraj = readobjs(OscFromAvgFile, 'n', 'o');

    // parse mean from osculating trajectory
    OBJS *avgFromOscTraj = readobjs(AvgFromOscFile, 'n', 'm');

    // convert osculating to mean elements
    oscList2Avg(oscTraj);

    // convert mean to osculating elements
    avgList2Osc(avgTraj);

    // compare mean elements
    printf("\nComparing transformation from osculating to mean elements:\n");
    meanErr(oscTraj, avgFromOscTraj);

    // compare mean elements
    printf("\nComparing transformation from mean to osculating elements:\n");
    oscErr(avgTraj, oscFromAvgTraj);

    // compare error in short period correction of mean elements
    printf("\nComparing error in mean element transformation from short period variations:\n");
    meanErr(avgTraj, avgFromOscTraj);

    // compare error in short period correction of osculating elements
    printf("\nComparing error in osculating element transformation from short period variations:\n");
    oscErr(oscTraj, oscFromAvgTraj);

}